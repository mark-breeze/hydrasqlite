﻿var lookupsViewModel = new App.ViewModel.Lookups();

// this is like jquery ready function
// (not supported in IE8 - but who gives...)
document.addEventListener("DOMContentLoaded", (): void => {

    lookupsViewModel.onLoad();
    ko.applyBindings(lookupsViewModel, document.getElementById("koContentBindingNode"));

    // handler to clear out object after editing...
    $("#editLookupModal").on("hidden.bs.modal", () => {
        lookupsViewModel.lookup.reset();
    });

    // set menu selected style
    $("#menuAdmin").addClass("active");

});