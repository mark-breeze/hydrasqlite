﻿var verifyEmailViewModel = new App.ViewModel.VerifyEmail();

document.addEventListener("DOMContentLoaded", (): void => {
    ko.applyBindings(verifyEmailViewModel, document.getElementById("koContentBindingNode"));
    verifyEmailViewModel.onLoad();
});