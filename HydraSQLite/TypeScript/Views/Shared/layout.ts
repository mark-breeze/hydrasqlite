﻿module App {

    export function showTimeoutLoginDialogue() {
        if (App.isShowingTimeoutLoginDialogue) return;
        App.isShowingTimeoutLoginDialogue = true;
        JsonWebApi.showWaiting(true);
        const url = appSettings.rootUrl + "SignedOut/TimeoutLoginDialogue";
        var xhr = new XMLHttpRequest();
        xhr.open("GET", url);
        //xhr.setRequestHeader("Content-type", "application/json");
        xhr.onload = () => {
            if (xhr.status < 200 || xhr.status >= 300) {
                // just bail right out
                window.location.href = appSettings.rootUrl + "SignedOut/Logout";
            }
            else {
                const text: string = xhr.responseText;
                $("#sessionTimeoutModalContainer").html(text);

                // bind the login
                const loginViewModel = new ViewModel.Login();
                loginViewModel.redirectUrl(""); // stay where we are...
                ko.applyBindings(loginViewModel, document.getElementById("sessionTimeoutModalContainer"));
                loginViewModel.onLoad();

                JsonWebApi.showWaiting(false);

                $("#sessionTimeoutModal").modal({
                    backdrop: "static",
                    keyboard: false,
                    show: true
                });
            }
        }
        xhr.onerror = () => {
            // just bail right out
            window.location.href = appSettings.rootUrl + "SignedOut/Logout";
        };
        xhr.send();
    }

    export function showBannerInfo() {
        // not using knockout binding here becuase it's outside the normal context
        const approveUsersLinkContainer = $("#approveUsersLink");
        const approveUsersLink = approveUsersLinkContainer.find("a");
        const approveUsersBadge = approveUsersLinkContainer.find("#approveUsersBadge");
        JsonWebApi.sendRequest(new JsonWebApi.Options({
            method: "GET",
            path: "Api/UserApi/GetAwaitingApprovalCount",
            success: (responseData) => {
                approveUsersLinkContainer.toggle(responseData.count > 0);
                approveUsersLink.attr("href", appSettings.rootUrl + "Admin/Users?q=awaitingApproval");
                approveUsersBadge.text(responseData.count.toString());
}
        }));
    }
}

document.addEventListener("DOMContentLoaded", () => {
    App.showBannerInfo();
});


