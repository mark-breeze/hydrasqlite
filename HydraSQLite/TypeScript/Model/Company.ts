﻿namespace App.Model {

    export class Company {

        constructor() {
            this.setUpComputeds();
            this.setUpValidation();
        }

        id: KnockoutObservable<string> = ko.observable("");
        name: KnockoutObservable<string> = ko.observable("");
        address1: KnockoutObservable<string> = ko.observable("");
        address2: KnockoutObservable<string> = ko.observable("");
        town: KnockoutObservable<string> = ko.observable("");
        county: KnockoutObservable<string> = ko.observable("");
        postcode: KnockoutObservable<string> = ko.observable("");
        dateEstablished: KnockoutObservable<Date> = ko.observable(new Date(App.emptyDateValue));
        lookupOneID: KnockoutObservable<number> = ko.observable(0);
        lookupTwoID: KnockoutObservable<number> = ko.observable(0);
        created: KnockoutObservable<Date> = ko.observable(new Date(App.emptyDateValue));
        createdByUserID: KnockoutObservable<string> = ko.observable(App.emptyGuidValue);
        lastUpdated: KnockoutObservable<Date> = ko.observable(new Date(App.emptyDateValue));
        lastUpdatedByUserID: KnockoutObservable<string> = ko.observable(App.emptyGuidValue);

        reset = () => {
            Utils.resetObject(this, {});
            this.createdByUserID(App.emptyGuidValue);
            this.lastUpdatedByUserID(App.emptyGuidValue);
        }

        private setUpComputeds = () => {
            // add some computed observables to help with select lists
            this.lookupOneID.forEditing = ko.computed({
                read: () => { return this.lookupOneID() === 0 ? undefined : this.lookupOneID(); },
                write: (newValue) => { newValue === undefined ? this.lookupOneID(0) : this.lookupOneID(newValue) }
            });
            this.lookupTwoID.forEditing = ko.computed({
                read: () => { return this.lookupTwoID() === 0 ? undefined : this.lookupTwoID(); },
                write: (newValue) => { newValue === undefined ? this.lookupTwoID(0) : this.lookupTwoID(newValue) }
            });
        }

        private setUpValidation = () => {
            // validation
            this.name.extend({
                validate: {
                    rule: () => { return !Utils.isEmptyOrWhitespace(this.name()); },
                    message: "Please enter a company name"
                }
            });

            this.address1.extend({
                validate: {
                    rule: () => { return !Utils.isEmptyOrWhitespace(this.address1()); },
                    message: "Please enter a first address line"
                }
            });
        }

        static mapping = {
            key(data) { return ko.utils.unwrapObservable(data.id); },
            create(options) {
                const company = new Company();
                ko.mapping.fromJS(options.data, {}, company);
                return company;
            }
            //, contacts: {
            //    key(data) { return ko.utils.unwrapObservable(data.id); },
            //    create(options) {
            //        var contact = new contact();
            //        ko.mapping.fromJS(options.data, {}, contact);
            //        return contact;
            //    }
            //}
        }
    }
}
