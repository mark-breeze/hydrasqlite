﻿namespace App.Model {

    export class UserToCreate {

        constructor() {
            this.setUpValidation();
            // trigger password validation when server message returns
            this.passwordServerValidationMessage.subscribe(() => {
                this.newPassword.validate();
            });
        }

        status: KnockoutObservable<App.UserStatus> = ko.observable(UserStatus.None);

        email: KnockoutObservable<string> = ko.observable("");
        newPassword: KnockoutObservable<string> = ko.observable("");
        repeatPassword: KnockoutObservable<string> = ko.observable("");
        passwordServerValidationMessage: KnockoutObservable<string> = ko.observable("");
        passwordValidationMessage: string = "";
        forename: KnockoutObservable<string> = ko.observable("");
        surname: KnockoutObservable<string> = ko.observable("");
        captchaID: KnockoutObservable<string> = ko.observable("");
        captchaText: KnockoutObservable<string> = ko.observable("");


        fromJsObject = (user: any) => {
            // NB - we're not setting the status here...
            user = user || { email: "", forename: "", surname: "" };
            this.email(user.email);
            this.newPassword("");
            this.repeatPassword("");
            this.forename(user.forename);
            this.surname(user.surname);
            this.captchaID("");
            this.captchaText("");
        }

        toJsObject = () => {
            return {
                email: this.email(),
                newPassword: this.newPassword(),
                forename: this.forename(),
                surname: this.surname(),
                captchaID: this.captchaID(),
                captchaText: this.captchaText()
            }
        }

        private setUpValidation = () => {
            this.forename.extend({
                validate: {
                    rule: () => { return !Utils.isEmptyOrWhitespace(this.forename()); },
                    message: "Please enter a forename"
                }
            });

            this.surname.extend({
                validate: {
                    rule: () => { return !Utils.isEmptyOrWhitespace(this.surname()); },
                    message: "Please enter a surname"
                }
            });

            this.email.extend({
                validate: {
                    rule: () => {
                        const validationOptions = this.email.validationOptions as ValidationOptions;
                        if (Utils.isEmptyOrWhitespace(this.email())) {
                            validationOptions.message = "Please supply an email address";
                            return false;
                        }
                        if (!Utils.isValidEmail(this.email())) {
                            validationOptions.message = "Email address is not valid";
                            return false;
                        }
                        return true;
                    }
                }
            });

            this.newPassword.extend({
                validate: {
                    rule: () => {
                        const isBlank = Utils.isEmptyOrWhitespace(this.newPassword());
                        const isTooShort = (!isBlank && this.newPassword().length < 8);
                        const isServerMessage = !Utils.isEmptyOrWhitespace(this.passwordServerValidationMessage());
                        this.passwordValidationMessage =
                            isServerMessage ? this.passwordServerValidationMessage() :
                            isBlank ? "Please enter a password" :
                            isTooShort ? "Password should be at least eight characters" : "";
                        return !(isBlank || isTooShort || isServerMessage);
                    },
                    message: () => {
                        return this.passwordValidationMessage;
                    }
                }
            });

            this.repeatPassword.extend({
                validate: {
                    rule: () => {
                        const isSomething = !Utils.isEmptyOrWhitespace(this.newPassword());
                        const isMatch = this.newPassword() !== this.repeatPassword();
                        const isOk = !(isSomething && isMatch);
                        return isOk;
                    },
                    message: "Passwords must match"
                }
            });

            this.captchaText.extend({
                validate: {
                    rule: () => { return !Utils.isEmptyOrWhitespace(this.captchaText()); },
                    message: "Please enter the code above"
                }
            });
        }
    }
}