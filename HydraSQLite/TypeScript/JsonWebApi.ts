﻿/// <reference path="./Typings/index.d.ts" />

// make sure appSettings.rootUrl and breeze.utils are defined and available
var appSettings = appSettings || {};
appSettings.rootUrl = appSettings.rootUrl || "";

// the main bit...
module App.JsonWebApi {

    //
    // -- class for defining what we want to do
    //
    export class Options {
        // convert passed parameters to strongly typed options object
        // and set defaults for missing options
        constructor(options: any) {
            this.path = typeof options.path === "undefined" ? "" : options.path;
            this.url = (typeof options.url === "undefined" ? (appSettings.rootUrl + options.path) : options.url);
            this.data = options.data;
            this.success = this.isFunction(options.success) ? options.success : this.success;
            this.failed = this.isFunction(options.failed) ? options.failed : this.failed;
            this.always = this.isFunction(options.always) ? options.always : this.always;
            this.unauthorised = this.isFunction(options.unauthorised) ? options.unauthorised : this.unauthorised;
            this.forbidden = this.isFunction(options.forbidden) ? options.unauthorised : this.forbidden;
            this.button = typeof options.button === "undefined" ? "" : options.button;
            this.loginPage = options.loginPage;
            this.method = options.method || "GET"; 
        }

        url: string;                     // full url
        path: string;                    // used with appSettings.rootUrl to make full url (if not supplied)
        data: any;                       // for posting
        success:(data: any) => void;     // do after if no error - returns data from API
        failed: () => void;              // do after if error
        always: () => void;              // do anyway
        unauthorised: () => void;        // do after if logged off
        forbidden: () => void;           // do after if not allowed
        button: HTMLInputElement;        // diable button during request to prevent clicking twice
        loginPage: string;               // redirect if not authenticated
        method: string;                  // GET or POST

        private isFunction(functionToCheck: any): boolean {
            const getType = {};
            return functionToCheck && getType.toString.call(functionToCheck) === "[object Function]";
        }
    }

    let retryAfterLogBackIn: Array<Options> = [];

    //
    // -- public interface
    //

    // helper function to make comon calls a bit more streamlined
    export function get(path: string, success: (data: any) => void, event?: Event) {
        JsonWebApi.showWaiting(true);
        const options = new Options({
            method: "GET",
            path: path,
            button: event == null ? undefined : (event.currentTarget != null ? event.currentTarget : event.srcElement),
            success: success,
            always: () => {
                JsonWebApi.showWaiting(false);
            }
        });
        sendRequest(options);
    }

    // helper function to make comon calls a bit more streamlined
    // data can be js object, ko viewmodel or JSON string
    export function post(path: string, data: any, success: (data: any) => void, event?: Event) {
        JsonWebApi.showWaiting(true);
        delete data.__ko_mapping__; // saves a bit of bandwidth
        if (typeof data === "object") {
            data = ko.toJS(data); // 'flatten' the observables           
        }
        if (typeof data !== "string") {
            data = JSON.stringify(data);
        }
        const options = new Options({
            method: "POST",
            path: path,
            data: data,
            button: event == null ? undefined : (event.currentTarget != null ? event.currentTarget : event.srcElement),
            success: success,
            always: () => {
                JsonWebApi.showWaiting(false);
            }
        });
        sendRequest(options);
    }

    // the full-fat version with all options available
    export function sendRequest(options: Options) {

        // bail out if the button is already disabled, else disable the button and carry on
        if (disableButton(options.button)) { return; }

        var xhr = new XMLHttpRequest();
        xhr.open(options.method, options.url);

        xhr.setRequestHeader("Content-type", "application/json");
        xhr.setRequestHeader("Accept", "application/json");

        xhr.onload = () => {
            if (xhr.status === 401) {
                // 401 = unauthorised = not logged in
                enableButton(options.button);
                reportError(xhr);
                if (isFunction(options.failed)) options.failed();
                retryAfterLogBackIn.push(options);
                App.showTimeoutLoginDialogue();
            }
            else if (xhr.status === 403) {
                // 403 = forbidden = logged in but not allowed
                enableButton(options.button);
                reportError(xhr);
                if (isFunction(options.failed)) options.failed();
                if (isFunction(options.always)) options.always();
            }
            else if (xhr.status < 200 || xhr.status >= 300) {
                enableButton(options.button);
                reportError(xhr);
                if (isFunction(options.failed)) options.failed();
                if (isFunction(options.always)) options.always();
            }
            else if (looksLikeHtml(xhr.responseText)) {
                enableButton(options.button);
                alert("Got unexpected API reponse - looks like HTML!");
                //reportError(xhr);
                if (isFunction(options.failed)) options.failed();
                if (isFunction(options.always)) options.always();
            }
            else {
                const text: string = xhr.responseText;
                try {
                    const data = JSON.parse(text);
                    processDates(data);
                    enableButton(options.button);
                    if (isFunction(options.success)) options.success(data);
                }
                catch (ex) {
                    enableButton(options.button);
                    let message = "???";
                    if (typeof ex === "string") { message = ex as string; }
                    if (ex && ex.message) { message = ex.message; }
                    reportError(xhr, message);
					ErrorHandler.reportJavascriptError(message, "Caught in JsonWebApi", 0, 0, ex);
                    if (isFunction(options.failed)) options.failed();
                }
                if (isFunction(options.always)) options.always();
            }
        }
        xhr.onerror = (e) => {
            alert("API Network Error ");
            console.dir(e);
            enableButton(options.button);
            reportError(xhr);
            if (isFunction(options.failed)) options.failed();
            if (isFunction(options.always)) options.always();
        };

        if (options.method === "POST" && !!options.data) {
            if (typeof options.data !== "string") {
                // convert opbject to json - if not already
                options.data = JSON.stringify(options.data);
            }
            xhr.send(options.data);
        }
        else {
            xhr.send();
        }
    }

    // for re-running any failed requests after logging back in
    export function onLoggedInCallback() {
        for (let i = 0; i < retryAfterLogBackIn.length; i++) {
            console.log(`*** RETRY: ${retryAfterLogBackIn[i].url}`);
        }
        runRetryStack();
    }

    //
    // -- busy indicator
    //

    export function showWaiting(shouldShow: boolean) {
        const spinnerTarget = document.getElementById("waitingSpinnerTarget");
        const blackout = document.getElementById("waitingBlackout");
        if (spinnerTarget == null) return;
        
        const isShowing = blackout.style.display === "block" || blackout.style.display === ""; 

        if (shouldShow && !isShowing) {
            const spinner = new Spinner({
                lines: 11 // The number of lines to draw
                , length: 0 // The length of each line
                , width: 29 // The line thickness
                , radius: 44 // The radius of the inner circle
                , scale: 1 // Scales overall size of the spinner
                , corners: 1 // Corner roundness (0..1)
                , color: "#666" // #rgb or #rrggbb or array of colors
                , opacity: 0.1 // Opacity of the lines
                , rotate: 0 // The rotation offset
                , direction: 1 // 1: clockwise, -1: counterclockwise
                , speed: 1 // Rounds per second
                , trail: 60 // Afterglow percentage
                , fps: 20 // Frames per second when using setTimeout() as a fallback for CSS
                , zIndex: 2e9 // The z-index (defaults to 2000000000)
                , className: "spinner" // The CSS class to assign to the spinner
                , top: "50%" // Top position relative to parent
                , left: "50%" // Left position relative to parent
                , shadow: false // Whether to render a shadow
                , hwaccel: true // Whether to use hardware acceleration
                , position: "fixed" // Element positioning
            });
            spinnerTarget.style.display = "block";
            blackout.style.display = "block";
            spinner.spin(spinnerTarget);
            $(spinnerTarget).data("spinner", spinner);
        }
        else if (!shouldShow && isShowing) {
            const spinner = $(spinnerTarget).data("spinner");
            if (spinner == null) {
                $(".spinner").remove(); // fallback, just in case 
            }
            else {
                spinner.stop();
            }
            spinnerTarget.style.display = "none";
            blackout.style.display = "none";
        }
    }

    //
    // -- private stuff...
    //

    function isFunction(fn:Function): boolean {
        const getType = {};
        return fn && getType.toString.call(fn) === "[object Function]";
    }

    function runRetryStack() {
        for (let i = 0; i < retryAfterLogBackIn.length; i++) {
            sendRequest(retryAfterLogBackIn[i]);
        }
        retryAfterLogBackIn = [];
    }

    // prevent buttons being pressed twice
    function disableButton(button: HTMLInputElement): boolean {
        if (button == null || typeof button.id === "undefined") {
            return false;
        }
        // if already disabled, return true so action can be cancelled
        if (button.disabled) {
            return true;
        }
        button.disabled = true;

        // if button has spinner, show it and hide its icon
        for (let i = 0; i < button.childNodes.length; i++) {
            const node = button.childNodes[i];
            const elem: HTMLElement = node.hasOwnProperty("className") ? <HTMLElement>node : undefined;
            if (elem && elem.className) {
                if (elem.className.indexOf("buttonIcon") > -1) {
                    elem.style.display = "none";
                }
                else if (elem.className.indexOf("buttonSpinner") > -1) {
                    elem.style.display = "inline-block";
                }
            }
        }

        if (!hasCssClass(button, "disabled")) {
            addCssClass(button, "disabled");
        }
        return false;
    }

    // need to call this after we're done
    function enableButton(button: HTMLInputElement) {
        if (button == null || typeof button.id === "undefined") {
            return;
        }
        button.disabled = false;
        
        // if button has spinner, hide it and show its icon
        for (let i = 0; i < button.childNodes.length; i++) {
            const node = button.childNodes[i];
            const elem: HTMLElement = node.hasOwnProperty("className") ? <HTMLElement>node : undefined;
            if (elem && elem.className) {
                if (elem.className.indexOf("buttonSpinner") > -1) {
                    elem.style.display = "none";
                }
                else if (elem.className.indexOf("buttonIcon") > -1) {
                    elem.style.display = "inline-block";
                }
            }
        }
        removeCssClass(button, "disabled");
    }

    function removeCssClass(elem: HTMLElement, cls: string) {
        const regex = new RegExp("(?:^|\\s)" + cls + "(?!\\S)", "g");
        elem.className = elem.className.replace(regex, "");
    }

    function hasCssClass(elem: HTMLElement, cls: string): boolean {
        const regex = new RegExp("(?:^|\\s)" + cls + "(?!\\S)", "g");
        const m = elem.className.match(regex);
        return !!m && m.length !== 0;
    }

    function addCssClass(elem: HTMLElement, cls: string) {
        elem.className = elem.className + " " + cls;
    }
    
    // show toastr message (or window containg response html if not json)
    function reportError(xhr: any, errorMessage?: string) {
        if (!!xhr && xhr.status === 403) {
            toastr.error("It looks like you have tried to do something you are not set up for!", "NOT ALLOWED");
            return;
        }
        if (!!xhr && xhr.status === 401) {
            toastr.error("Could not process your request becuase you are not logged in or your session expired.", "Not logged in!");
            return;
        }
        if (!!xhr && xhr.status === 404) {
            toastr.error("The API method could not be found!", "ERROR");
            return;
        }
        if (!xhr || !(xhr.responseText)) {
            return;
        }
        if (isJson(xhr.responseText)) {
            const json = JSON.parse(xhr.responseText);
            if (errorMessage) {
                toastr.error(errorMessage, "ERROR");
            }
            else if (json.ExceptionMessage) {
                toastr.error(json.ExceptionMessage, "ERROR");
            }
            else if (json.Message) {
                toastr.error(json.Message, "ERROR");
            }
            else {
                toastr.error(json, "ERROR");
            }
        }
        else {
            // show yellow screen of death (?) if not json response
            const popUpWindow = window.open("about:blank", "_blank", "height=400,width=600,toolbar=no,menubar=no,scrollbars=yes,resizable=yes,location=no,directories=no,status=no");
            popUpWindow.document.write(xhr.responseText);
        }
    }

    // for deciding what to do in error reporting
    function isJson(json:any):boolean {
        try { JSON.parse(json); }
        catch (e) { return false; }
        return true;
    }

    // currenty web api methods are returning HTML pages for 403s etc.!
    function looksLikeHtml(data: any): boolean {
        if (data == null || typeof data !== "string" || data.length < 10) return false;
        const curlyAt = (data as string).indexOf("{");
        const angleAt = (data as string).indexOf("<");
        return angleAt !== -1 && (angleAt < curlyAt || curlyAt === -1);
    }
    
    // recurse through response objects looking for ISO 8601 dates - and parse them
    function processDates(data: any) {
        for (let item in data) {
            if (data.hasOwnProperty(item)) {
                if (data[item] !== null) {
                    const type: string = typeof data[item];
                    if (type === "string") {
                        const txt = data[item] as string;
                        // seven or eight digit numbers are actually valid ISO 8601 formats - but we want to ignore those
                        if (isNaN(+txt)) {
                            const m = moment(txt, moment.ISO_8601, true);
                            if (m.isValid()) {
                                if (txt.substring(0, 4) === "0001") {
                                    data[item] = new Date(-62135596800000); // 01/01/0001 means 'no value'
                                }
                                data[item] = m.toDate(); // could leave as moment?
                            }
                        }
                    }
                    else if (type === "object") {
                        processDates(data[item]);
                    }
                }
            }
        }
    }
}

