﻿namespace App.ViewModel {

    enum ResetPasswordState {
        None = 0,
        Ready = 1,
        Expired = 2,
        Success = 3,
        Failed = 4
    }

    export class ResetPassword {
    
        constructor() {
            this.isMessage = ko.computed({
                owner: this, read: () => {
                    return this.message() !== "" && this.message().indexOf("ok") !== 0;
                }
            });
            this.isValidationMessage = ko.computed({
                owner: this, read: () => {
                    return this.validationMessage() !== "" && this.validationMessage().indexOf("ok") !== 0;
                }
            });
        }

        //
        // -- properties
        //

        key: KnockoutObservable<string> = ko.observable("");
        state: KnockoutObservable<ResetPasswordState> = ko.observable(0);
        email: KnockoutObservable< string > = ko.observable("");       
        password: KnockoutObservable<string> = ko.observable("");
        passwordRepeat: KnockoutObservable<string> = ko.observable("");
        message: KnockoutObservable<string> = ko.observable("");
        validationMessage: KnockoutObservable<string> = ko.observable("");

        // computed
        isValidationMessage: KnockoutComputed<boolean>;
        isMessage: KnockoutComputed<boolean>;

        //
        // -- methods
        //

        // call when DOM loaded
        onLoad = () => {

            JsonWebApi.showWaiting(true);

            this.key(this.getLastPartOfUrlPath());
            var postData = ko.toJSON(this);

            JsonWebApi.sendRequest(new JsonWebApi.Options({
                method: "POST",
                data: postData,
                path: "Api/LoginApi/ResetPasswordInit",
                success: responseData => {
                    ko.mapping.fromJS(responseData, {}, this);
                },
                always: () => {
                    JsonWebApi.showWaiting(false);
                }
            }));
        };

        submit = (data: any, event: Event) => {
            this.validate();
            if (this.isValidationMessage()) {
                return;
            }
            
            JsonWebApi.showWaiting(true);
            JsonWebApi.sendRequest(new JsonWebApi.Options({
                method: "POST",
                path: "Api/LoginApi/ResetPassword",
                data: ko.toJSON(this),
                button: (event.currentTarget) ? event.currentTarget : event.srcElement,
                success: responseData => {
                    ko.mapping.fromJS(responseData, {}, this);
                },
                always: () => {
                    JsonWebApi.showWaiting(false);
                }
            }));
        };

        //
        // -- private 
        //

        private getLastPartOfUrlPath():string {
            const url = document.URL;
            if (!url) return ("");
            const parts = url.split("/");
            if (parts.length === 0) return ("");
            return parts[parts.length - 1];
        }
        
        private validate() {
            let msg = "";
            if (!this.password() || this.password().length === 0)
                msg += "Password is required!";
            else if (this.password().length < 8)
                msg += "Password must be longer than eight characters";
            else if (this.password() !== this.passwordRepeat())
                msg += "Passwords must match";
            this.validationMessage(msg);
        };
    }
}
