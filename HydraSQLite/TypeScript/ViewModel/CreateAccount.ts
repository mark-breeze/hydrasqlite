﻿namespace App.ViewModel {

    export class CreateAccount {

        user = new Model.UserToCreate();

        shouldShowForm: KnockoutObservable<boolean> = ko.observable(true);
        pendingMessageCss: KnockoutObservable<string> = ko.observable("");
        pendingMessageHeading: KnockoutObservable<string> = ko.observable("");
        pendingMessageBody: KnockoutObservable<string> = ko.observable("");
        
        validationMessage: KnockoutObservable<string> = ko.observable("");
        shouldShowValidationMessage: KnockoutComputed<boolean> = ko.computed(() => !Utils.isEmptyOrWhitespace(this.validationMessage()));

        canResend: KnockoutComputed<boolean> = ko.computed(() => this.user.status() === UserStatus.WaitingEmailVerification);

        onLoad = () => {
            this.loadStored();
            this.loadStatus();
            this.user.newPassword.subscribe(this.onPasswordChanged); 
        }
        
        submit = (data: any, event: Event) => {           
            Validation.shouldShowMessages(this.user, true);
            Validation.validate(this.user);
            if (!Validation.isValid(this.user)) {
                this.validationMessage("Please fix the highlighted problems above.");
                return;
            }
            var postData = this.user.toJsObject();
            JsonWebApi.post("Api/CreateAccountApi/StartCreateAccount", postData, (responseData) => {
                if (responseData.captchaWrong) {
                    this.loadCaptcha();
                    this.validationMessage("The code was wrong.  Please try again.");
                }
                else if (responseData.captchaExpired) {
                    this.loadCaptcha();
                    this.validationMessage("The code had expired. Please try again.");
                }
                else if (responseData.isInvalidPassword) {
                    this.validationMessage(responseData.invalidPasswordMessage);
                }
                else {
                    this.user.status(responseData.status);
                    this.saveStored();
                    this.setPendingMessages(true, !responseData.sentEmail);
                    this.loadCaptcha();
                }
                if (!responseData.sentEmail && responseData.status === UserStatus.WaitingEmailVerification) {
                    toastr.warning("Sending verification email failed");
                }
            }, event);
        }

        resendEmail = (data: any, event: Event) => {
            JsonWebApi.get(`Api/CreateAccountApi/ResendVerificationEmail?email=${this.user.email()}`, (responseData) => {
                this.setPendingMessages(false, !responseData.isOk);
            });
        }

        reset = (data: any, event: Event) => {
            this.user.fromJsObject({});
            this.user.status(UserStatus.None);
            this.saveStored();
            this.setPendingMessages(false);
        }

        //
        // -- private
        //

        private loadStatus = () => {
            JsonWebApi.get(`Api/CreateAccountApi/GetStatus?email=${this.user.email()}`, (responseData) => {
                this.user.status(responseData.status);
                this.setPendingMessages(false);
                this.loadCaptcha();
            });
        }

        private loadStored = () => {
            if (typeof (Storage) !== "undefined") {
                const json = localStorage.getItem("createAccount");
                if (json) {
                    try {
                        this.user.fromJsObject(JSON.parse(json));
                    } catch (e) {
                        // meh.
                        this.user.fromJsObject({});
                    }
                }
            }
        }
        
        private saveStored = () => {
            if (typeof (Storage) !== "undefined") {
                localStorage.setItem("createAccount", JSON.stringify(this.user.toJsObject()));
            }
        }

        private loadCaptcha = () => {
            JsonWebApi.get("Api/CreateAccountApi/CreateCaptcha", (responseData) => {
                if (responseData.message === "ok") {
                    this.user.captchaID(responseData.id);
                    const div = document.getElementById("captchaImage");
                    div.innerHTML = "";
                    const img = new Image();
                    img.src = appSettings.rootUrl + "Api/CreateAccountApi/CaptchaImage/" + this.user.captchaID();
                    div.appendChild(img);
                    this.user.captchaText("");
                }
                else {
                    toastr.error("Failed to initiialise captcha.", "Captcha Error");
                }
            });
        }

        private setPendingMessages = (didJustSubmit: boolean, emailFailed:boolean = false) => {
            const status = this.user.status();
            if (status === UserStatus.Active && didJustSubmit) {
                this.pendingMessageCss("callout callout-warning");
                this.pendingMessageHeading("Hmmm...");
                this.pendingMessageBody("It looks lioke there's an account already registered with that email.");
                this.shouldShowForm(false);
            }
            else if (status === UserStatus.WaitingEmailVerification && didJustSubmit) {
                this.pendingMessageHeading("Thank you");
                if (emailFailed) {
                    this.pendingMessageCss("callout callout-warning");
                    this.pendingMessageBody("We've got your details but sending a verification email failed.  If you're sure the email is correct, plase try re-sending.");
                }
                else {
                    this.pendingMessageCss("callout callout-success");
                    this.pendingMessageBody("We've sent you a verification email message and you need to follow the link in the message to continue.  Check your spam filter if you did not see it or try re-sending.");
                }
                this.shouldShowForm(false);
            }
            else if (status === UserStatus.WaitingEmailVerification) {
                this.pendingMessageHeading("Email Verification");
                if (emailFailed) {
                    this.pendingMessageCss("callout callout-warning");
                    this.pendingMessageBody("We could not send a verification.  Please try re-sending or start afresh with a different email address.");
                }
                else {
                    this.pendingMessageCss("callout callout-success");
                    this.pendingMessageBody("You need to click on the verification link sent via email.  Check your spam filter if you did not see it.");
                }
                
                this.shouldShowForm(false);
            }
            else if (status === UserStatus.WaitingApproval) {
                this.pendingMessageCss("callout callout-success");
                this.pendingMessageHeading("Awaiting Approval");
                this.pendingMessageBody("We've already got your submission.  Please wait while we approve it.");
                this.shouldShowForm(false);
            }
            else {
                this.pendingMessageHeading("- - -");
                this.pendingMessageBody("- - -");
                this.shouldShowForm(true);
            }
        }

        private onPasswordChanged = Utils.debounce(() => {
            // don't send pwd over insecure external network
            if (window.location.protocol !== "https:" && location.hostname !== "localhost") return;
            JsonWebApi.sendRequest(new JsonWebApi.Options({
                method: "GET",
                path: `Api/UserApi/ValidatePassword?password=${encodeURI(this.user.newPassword())}`,
                success: responseData => {
                    this.user.passwordServerValidationMessage(responseData.isOkay ? "" : responseData.message);
                }
            }));
        }, 200);

    }
}