﻿// add extra computed to allow 
interface KnockoutObservable<T> {
    forEditing: KnockoutComputed<T>;
}
