﻿// dirty flag for observables
interface KnockoutExtenders {
    trackChange: (target: KnockoutObservable<any>, track: boolean) => KnockoutObservable<any>; // dirty flag
}

// extra fields for validation
interface KnockoutObservable<T> {
    isDirty: KnockoutObservable<boolean>;
    originalValue: T;
}