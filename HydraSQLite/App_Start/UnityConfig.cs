using System;
using HydraSQLite.App;
using HydraSQLite.Services;
using Unity;
using Unity.Lifetime;
using HydraSQLite.Services.Mockable;
using HydraSQLite.Services.BusinessLogic;

namespace HydraSQLite {

    /// <summary>
    /// Unity configuration:
    ///   - Create instance of container
    ///   - Register services (or types thereof) that we'll want to inject
    /// </summary>
    public static class UnityConfig {

        #region " Unity Container "

        private static Lazy<IUnityContainer> myContainer = new Lazy<IUnityContainer>(() => {
            var container = new UnityContainer();
            RegisterTypes(container);
            return container;
        });

        /// <summary>
        /// Configured Unity Container.
        /// </summary>
        public static IUnityContainer Container => myContainer.Value;

        #endregion

        /// <summary>
        /// Registers the type mappings with the Unity container.
        /// </summary>
        public static void RegisterTypes(IUnityContainer container) {

            // https://docs.microsoft.com/en-us/previous-versions/msp-n-p/ff660872(v=pandp.20)
            //  - TransientLifetimeManager = new instances every time
            //  - ContainerControlledLifetimeManager = singleton - similarish to having repos as static classes
            //  - HierarchicalLifetimeManager - like singletons (ContainerControlledLifetimeManager) but creates new instances for child container objects

            container.RegisterType<ICurrentUserIdentity, CurrentUserIdentity>(new ContainerControlledLifetimeManager());
            container.RegisterType<ICache, MemoryCacheWrapper>(new ContainerControlledLifetimeManager());
            container.RegisterType<IBreezeConfiguration, BreezeConfiguration>(new HierarchicalLifetimeManager());
            container.RegisterType<IEmailer, Emailer>(new HierarchicalLifetimeManager());
            container.RegisterType<ICommonPasswordRepository, CommonPasswordRepository>(new HierarchicalLifetimeManager());
            container.RegisterType<ILookupGroupRepository, LookupGroupRepository>(new HierarchicalLifetimeManager());
            container.RegisterType<IUserRepository, UserRepository>(new HierarchicalLifetimeManager());
            container.RegisterType<IEmailUrlKey, EmailUrlKey>(new ContainerControlledLifetimeManager());
            container.RegisterType<ICompanyRepository, CompanyRepository>(new HierarchicalLifetimeManager());
            container.RegisterType<ILogin, Login>(new HierarchicalLifetimeManager());
            container.RegisterType<ICreateAccount, CreateAccount>(new HierarchicalLifetimeManager());
            container.RegisterType<IPasswordLogic, PasswordLogic>(new HierarchicalLifetimeManager());
            container.RegisterType<IUserLogic, UserLogic>(new HierarchicalLifetimeManager());
            container.RegisterType<ICompanyLogic, CompanyLogic>(new HierarchicalLifetimeManager());
        }
    }
}