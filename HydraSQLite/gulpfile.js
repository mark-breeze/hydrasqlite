﻿/// <binding BeforeBuild='default' />

var gulp = require("gulp");

// Cherry-pick the files we want on the server from the node_modules folder
// (Note that they will be bundled and minified by ASP.NET)
gulp.task("copy-assets", function () {

    gulp.src("./node_modules/@types/**/*.d.ts").pipe(gulp.dest("./TypeScript/Typings"));

    // There's a hack in the moment typings file to make it work without module system - so don't re-copy from node_modules for now...
    // gulp.src("./node_modules/moment/moment.d.ts").pipe(gulp.dest("./TypeScript/Typings/moment"));

    gulp.src("./node_modules/jquery/dist/jquery.js").pipe(gulp.dest("./Assets/lib/jquery"));

    gulp.src("./node_modules/bootstrap/dist/**/*").pipe(gulp.dest("./Assets/lib/bootstrap"));

    gulp.src("./node_modules/bootbox/bootbox.js").pipe(gulp.dest("./Assets/lib/bootbox"));

    gulp.src("./node_modules/knockout/build/output/knockout-latest.debug.js").pipe(gulp.dest("./Assets/lib/knockout"));
    gulp.src("./node_modules/knockout-mapping/dist/knockout.mapping.js").pipe(gulp.dest("./Assets/lib/knockout-mapping"));

    gulp.src("./node_modules/font-awesome/fonts/**/*").pipe(gulp.dest("./Assets/lib/font-awesome/fonts"));
    gulp.src("./node_modules/font-awesome/css/**/*").pipe(gulp.dest("./Assets/lib/font-awesome/css"));

    gulp.src("./node_modules/toastr/toastr.js").pipe(gulp.dest("./Assets/lib/toastr"));
    gulp.src("./node_modules/toastr/build/toastr.css").pipe(gulp.dest("./Assets/lib/toastr"));

    gulp.src("./node_modules/spin/dist/spin.js").pipe(gulp.dest("./Assets/lib/spin"));

    gulp.src("./node_modules/moment/moment.js").pipe(gulp.dest("./Assets/lib/moment"));
});

// The default task (called when you run 'gulp' from cli)
//  - The second argument makes sure other tasks are run first.
gulp.task("default", ["copy-assets"]);