﻿using System;
using System.Drawing.Imaging;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Http;
using HydraSQLite.App;
using HydraSQLite.Model.Api;
using HydraSQLite.Services;
using HydraSQLite.Services.Mockable;

// Stop ReSharper moaning about dependency injection properties:
// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace HydraSQLite.Controllers.Api {

    public class CreateAccountApiController : ApiController {

        #region " dependencies "

        [Unity.Attributes.Dependency]
        public ICreateAccount CreateAccount { get; set; }
        
        [Unity.Attributes.Dependency]
        public IBreezeConfiguration Config { get; set; }
        
        [Unity.Attributes.Dependency]
        public IEmailer Emailer { get; set; }

        [Unity.Attributes.Dependency]
        public ICache Cache { get; set; }

        [Unity.Attributes.Dependency]
        public IEmailUrlKey EmailUrlKey { get; set; }

        [Unity.Attributes.Dependency]
        public IUserRepository UserRepository { get; set; }

        #endregion
        
        [AcceptVerbs("GET")]
        public object GetStatus([FromUri] string email) {
            return new {
                status = CreateAccount.GetStatus(email)
            };
        }

        [AcceptVerbs("POST")]
        public async Task<CreateAccountResponse> StartCreateAccount(CreateAccountRequest request) {
            return await CreateAccount.StartCreateAccount(new UrlHelperWebApiAdaptor(Url), request);
        }

        [AcceptVerbs("GET")]
        public object ResendVerificationEmail([FromUri] string email) {
            // to (maybe) do: check to see how many requests already sent?
            return new {
                isOk = CreateAccount.ResendVerificationEmail(new UrlHelperWebApiAdaptor(Url), email)
            };
        }


        [AcceptVerbs("GET")]
        public object VerifyEmail([FromUri] string key) {
            var result = CreateAccount.VerifyEmail(key);
            return new {
                isOk = result == VerifyEmailResult.Success,
                keyFailed = result == VerifyEmailResult.KeyLookupFailed || result == VerifyEmailResult.EmailLookupFailed,
                unexpectedUserStatus = result == VerifyEmailResult.UnexpectedUserStatus
            };
        }

        [AcceptVerbs("GET")]
        public HttpResponseMessage CreateCaptcha() {
            var captcha = new CaptchaImage();
            // TODO - expiry via config setting
            Cache.Set(captcha.UniqueId, captcha,  DateTimeOffset.Now.AddMinutes(5));
            return Request.CreateResponse(HttpStatusCode.OK, new {
                message = "ok",
                id = captcha.UniqueId
            });
        }

        [AcceptVerbs("GET")]
        public HttpResponseMessage CaptchaImage([FromUri]string id) {
            if (!Cache.Contains(id)) {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Expired!");
            }
            var captcha = Cache.Get(id) as CaptchaImage;
            if (captcha == null) {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Expired!");
            }
            var bmp = captcha.RenderImage();
            var ms = new MemoryStream();
            bmp.Save(ms, ImageFormat.Png);
            var result = new HttpResponseMessage(HttpStatusCode.OK) {
                Content = new ByteArrayContent(ms.ToArray())
            };
            result.Content.Headers.ContentType = new MediaTypeHeaderValue("image/png");
            return result;
        }

        [AcceptVerbs("GET"), Route("api/ContactApi/VerifyCaptcha")]
        public HttpResponseMessage VerifyCaptcha(string id, string text) {
            if (!Cache.Contains(id)) {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Expired!");
            }
            if (!(Cache.Get(id) is CaptchaImage captcha)) {
                return Request.CreateErrorResponse(HttpStatusCode.NotFound, "Expired!");
            }
            return Request.CreateResponse(HttpStatusCode.OK, new {
                message = text == captcha.Text ? "ok" : "wrongText"
            });
        }

    }
}

