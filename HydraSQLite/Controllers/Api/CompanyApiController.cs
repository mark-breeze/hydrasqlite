﻿using System;
using System.Linq;
using System.Web.Http;
using HydraSQLite.App;
using HydraSQLite.Model.Api;
using HydraSQLite.Services;
using HydraSQLite.Services.BusinessLogic;

// Stop ReSharper moaning about dependency injection properties:
// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace HydraSQLite.Controllers.Api {

    [AuthoriseApi(Model.UserRole.Basic)]
    public class CompanyApiController : ApiController {

        #region " dependencies "

        [Unity.Attributes.Dependency]
        public ILookupGroupRepository LookupGroupRepository { get; set; }

        [Unity.Attributes.Dependency]
        public ICompanyRepository CompanyRepository { get; set; }

        [Unity.Attributes.Dependency]
        public ICompanyLogic CompanyLogic { get; set; }

        #endregion
        
        [AcceptVerbs("GET")]
        public object GetLookups() {
            return new {
                lookupOnes = LookupGroupRepository.GetLookup(Model.Lookups.Group.LookupOne).Items.Where(itm => !itm.IsArchived),
                lookupTwos = LookupGroupRepository.GetLookup(Model.Lookups.Group.LookupTwo).Items.Where(itm => !itm.IsArchived)
            };
        }

        [AcceptVerbs("POST")]
        public object Search(CompanySearchParameters searchParms) {
            var results = CompanyRepository.Search(searchParms);
            return new {
                message = "ok",
                totalCount = results.count,
                companies = results.list
            };
        }
        
        [AcceptVerbs("GET")]
        public Model.Company Load([FromUri]Guid id) {
            return CompanyRepository.Load(id);
        }

        [AcceptVerbs("POST")]
        public object Save(Model.Company company) {
            CompanyLogic.PreSave(company);
            CompanyRepository.Save(company);
            return new { message = "ok" };
        }

        [AcceptVerbs("POST")]
        public object Delete(Model.Company company) {
            CompanyRepository.Delete(company);
            return new {
                message = "ok",
                error = ""
            };
        }
    }
}
