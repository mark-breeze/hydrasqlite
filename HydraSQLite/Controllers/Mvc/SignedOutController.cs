﻿using System.Web.Mvc;

namespace HydraSQLite.Controllers.Mvc {

    public class SignedOutController : Controller {

        [HttpGet]
        public ActionResult Index() {
            return View();
        }

        [HttpGet]
        public ActionResult Error() {
            return View();
        }

        [HttpGet]
        public ActionResult Forbidden() {
            return View();
        }

        [HttpGet]
        public ActionResult ResetPassword(string key) {
            return View();
        }

        [HttpGet]
        public ActionResult VerifyEmail(string key) {
            return View();
        }


        [HttpGet]
        public ActionResult PasswordConvert() {
            return View();
        }
        
        // fragment of HTML for showing login modal
        [HttpGet]
        public ActionResult TimeoutLoginDialogue() {
            return View();
        }

        [HttpGet]
        public ActionResult Logout() {
            System.Web.Security.FormsAuthentication.SignOut();
            return RedirectToAction("Index", "SignedOut");
        }
    }
}
