﻿using System;
using System.Web.Mvc;
using HydraSQLite.Model.Lookups;
using HydraSQLite.Services;

namespace HydraSQLite.Controllers.Mvc {

    [App.AuthoriseMvc(Model.UserRole.Admin)]
    public class AdminController : Controller {

        private readonly ILookupGroupRepository myLookupRepo;

        public AdminController(ILookupGroupRepository lookupRepository) {
            myLookupRepo = lookupRepository;
        }

        public ActionResult Users() {
            return View();
        }

        public ActionResult Lookups(string id) {
            Group group;
            try {
                group = (Group)Enum.Parse(typeof(Group), id, true);
            }
            catch {
                // dodgy url - just redirect
                return RedirectToAction("Index", "Home");
            }

            var lu = myLookupRepo.GetLookup(group);
            if (lu == null)
                return RedirectToAction("Index", "Home");

            ViewData["lookupDescription"] = lu.Description;

            ViewData["lookupGroup"] = (int)group;
            return View();
        }

    }
}

