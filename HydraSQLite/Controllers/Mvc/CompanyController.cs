﻿using System.Web.Mvc;

namespace HydraSQLite.Controllers.Mvc {

    public class CompanyController : Controller {

        public ActionResult Index() {
            return RedirectToAction("Search");
        }

        public ActionResult Search() {
            return View();
        }
    }
}