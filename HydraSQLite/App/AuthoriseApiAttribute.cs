﻿using System;
using System.Linq;
using System.Net.Http;
using System.Web.Mvc;
using HydraSQLite.Model;
using HydraSQLite.Services;
using HydraSQLite.Services.Mockable;

namespace HydraSQLite.App {

    public class AuthoriseApiAttribute : System.Web.Http.AuthorizeAttribute {

        // can't use dependency injection (?)
        private IBreezeConfiguration Config { get; } = new BreezeConfiguration();

        private UserRole[] AcceptedRoles { get; } 

        public AuthoriseApiAttribute() {
            AcceptedRoles = new [] { UserRole.Basic };
        }

        public AuthoriseApiAttribute(params UserRole[] acceptedRoles) {
            AcceptedRoles = acceptedRoles;
        }
        
        public override void OnAuthorization(System.Web.Http.Controllers.HttpActionContext actionContext) {

            //If role == LoggedOut, means anyone can access. Don't need to be logged in.
            if (AcceptedRoles.Any(role => role == UserRole.LoggedOut)) {
                return;
            }
            var login = DependencyResolver.Current.GetService(typeof(ILogin)) as ILogin;
            var user = login?.CurrentUser;
            if (user == null || user.ID == Guid.Empty) {
                actionContext.Response = new HttpResponseMessage(System.Net.HttpStatusCode.Unauthorized);
            }
            else if (!AuthoriseRoleChecker.IsInRole(user, AcceptedRoles)) {
                actionContext.Response = new HttpResponseMessage(System.Net.HttpStatusCode.Forbidden);
            }
        }


    }
}