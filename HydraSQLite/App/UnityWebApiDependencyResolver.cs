﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Web.Http.Dependencies;
using Unity;
using Unity.Exceptions;

namespace HydraSQLite.App {

    public class UnityWebApiDependencyResolver : IDependencyResolver {

        private readonly IUnityContainer myContainer;
        private readonly SharedDependencyScope mySharedScope;
        
        public UnityWebApiDependencyResolver(IUnityContainer container) {
            myContainer = container ?? throw new ArgumentNullException(nameof(container));
            mySharedScope = new SharedDependencyScope(container);
        }

        public IDependencyScope BeginScope() {
            return mySharedScope;
        }
        
        public object GetService(Type serviceType) {
            try {
                return myContainer.IsRegistered(serviceType) ? myContainer.Resolve(serviceType) : null;
            }
            catch (ResolutionFailedException ex) {
                Debug.WriteLine("Resolution failed: " + serviceType);
                Debug.WriteLine(ex.ToString());
                return null;
            }
        }

        public IEnumerable<object> GetServices(Type serviceType) {
            try {
                return myContainer.ResolveAll(serviceType);
            }
            catch (ResolutionFailedException ex) {
                Debug.WriteLine("Resolution failed: " + serviceType);
                Debug.WriteLine(ex.ToString());
                // don't throw exceptions for unknown types
                return new List<object>();
            }
        }

        public void Dispose() {
            myContainer.Dispose();
            mySharedScope.Dispose();
        }
    }


    public class SharedDependencyScope : IDependencyScope {

        private readonly IUnityContainer myContainer;

        public SharedDependencyScope(IUnityContainer container) {
            myContainer = container;
        }

        public object GetService(Type serviceType) {
            return myContainer.Resolve(serviceType);
        }

        public IEnumerable<object> GetServices(Type serviceType) {
            return myContainer.ResolveAll(serviceType);
        }

        public void Dispose() {
            // do nothing since the container is shared.
        }
    }
}