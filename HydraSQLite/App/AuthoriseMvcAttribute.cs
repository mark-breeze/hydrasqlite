﻿using System.Linq;
using System.Web.Mvc;
using HydraSQLite.Model;
using HydraSQLite.Services;
using HydraSQLite.Services.Mockable;

namespace HydraSQLite.App {

    /// <inheritdoc />
    /// <summary>
    /// Used to deny access to various functions of the app based on user's role
    /// </summary>
    public class AuthoriseMvcAttribute : AuthorizeAttribute {

        // can't use dependency injection
        private IBreezeConfiguration Config { get; } = new BreezeConfiguration();

        private UserRole[] AcceptedRoles { get; }

        public AuthoriseMvcAttribute(params UserRole[] acceptedRoles) {
            AcceptedRoles = acceptedRoles;
        }

        // redirect if not authorised or not authenticated
        //  - alternatively could return 401 or 403:
        //      - filterContext.Response = new HttpResponseMessage(System.Net.HttpStatusCode.Forbidden);
        //      - filterContext.Response = new HttpResponseMessage(System.Net.HttpStatusCode.Unauthorized);
        public override void OnAuthorization(AuthorizationContext filterContext) {
            //If role == LoggedOut, means don't need to be logged in.
            if (AcceptedRoles.Any(role => role == UserRole.LoggedOut)) {
                return;
            }
            var login = DependencyResolver.Current.GetService(typeof(ILogin)) as ILogin;
            var user = login?.CurrentUser;

            // does user have one of the roles?
            if (user == null || !AuthoriseRoleChecker.IsInRole(user, AcceptedRoles)) {
                var urlHelper = new UrlHelper(filterContext.RequestContext);
                filterContext.Result = new RedirectResult(urlHelper.Action("Index", "SignedOut", null));
            }
        }
    }
}