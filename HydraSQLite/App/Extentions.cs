﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Linq;
using System.Reflection;
using System.Text;

namespace HydraSQLite.App {

    internal static class Extentions {

        #region Lists

        /// <summary>
        /// bang in a variable and a list of values you want to check to see if it contains the value in question 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="source">the variable you're looking for in the list i.e. 2 or "blah"</param>
        /// <param name="list">the list you're looking at to see if your variable is contained in it i.e. 2,4,6,8 or "blah","blee","bloo"</param>
        /// <returns></returns>
        public static bool In<T>(this T source, params T[] list) {
            // if the source is null return false
            if (null == source) return false;
            return list.Contains(source);
        }

        #endregion

        #region DateTime

        public static bool IsValidSqlDate(this DateTime dateTime) {
            return dateTime > System.Data.SqlTypes.SqlDateTime.MinValue.Value && dateTime <= System.Data.SqlTypes.SqlDateTime.MaxValue.Value;
        }

        public static int AgeYears(this DateTime dateTime) {
            var today = DateTime.Now;
            var age = today.Year - dateTime.Year;
            if (today.DayOfYear < dateTime.DayOfYear)
                age--;
            return age;
        }

        // make sure it's local time
        public static object ToDatabaseValue(this DateTime dateTime) {
            if (!IsValidSqlDate(dateTime)) {
                return DBNull.Value;
            }
            // if already set to local, assume is correct value
            if (dateTime.Kind == DateTimeKind.Local) {
                return dateTime;
            }
            // if not local, add correct offset to convert UTC to local time
            //   - e.g., will be 0 for GMT or +1h for BST
            var offset = TimeZoneInfo.Local.GetUtcOffset(dateTime);
            return DateTime.SpecifyKind(dateTime, DateTimeKind.Local) + offset;
        }

        #endregion

        #region " Int 32 "

        public static object ToDatabaseID(this int value) {
            return value > 0 ? value : (object)DBNull.Value;
        }

        // 1st, 2nd, 3rd, etc.
        public static string AddOrdinal(this int number) {
            switch (number % 100) {
                case 11:
                case 12:
                case 13: return number + "th";
            }
            switch (number % 10) {
                case 1: return number + "st";
                case 2: return number + "nd";
                case 3: return number + "rd";
                default: return number + "th";
            }
        }

        #endregion

        #region " String "

        public static float ToFloat(this string text) {
            return !float.TryParse(text, out var num) ? 0.0f : num;
        }

        public static double ToDouble(this string text) {
            return !double.TryParse(text, out var num) ? 0.0d : num;
        }

        public static int ToInt(this string text) {
            return !int.TryParse(text, out var num) ? 0 : num;
        }

        public static bool ToBool(this string text) {
            text = text.Trim().ToLower();
            return text == "1" || text == "true" || text == "yes";
        }

        public static DateTime ToDateTime(this string text) {
            var value = DateTime.MinValue;
            if (string.IsNullOrWhiteSpace(text))
                return value;
            DateTime.TryParse(text, out value);
            return value;
        }

        // convert null to DBNull to that parameters are not removed
        public static object ToDatabaseValue(this string value) => (object)value ?? DBNull.Value;

        /// <summary>Replaces first instance of searched for text.</summary>
        public static string ReplaceFirst(this string text, string searchFor, string replaceWith, StringComparison stringComparison) {
            var pos = text.IndexOf(searchFor, stringComparison);
            if (pos < 0) {
                return text;
            }
            return text.Substring(0, pos) + replaceWith + text.Substring(pos + searchFor.Length);
        }

        /// <summary>Removes all white space from within string.</summary>
        public static string RemoveAllWhiteSpace(this string input) {
            var output = new StringBuilder();
            foreach (var character in input.Trim()) {
                if (!char.IsWhiteSpace(character)) {
                    output.Append(character);
                }
            }
            return output.ToString();
        }

        /// <summary>Removes all non-alphanumeric characters from within string.</summary>
        public static string RemoveAllNonAlphanumeric(this string input) {
            var output = new StringBuilder();
            foreach (var character in input.Trim()) {
                if (char.IsLetterOrDigit(character)) {
                    output.Append(character);
                }
            }
            return output.ToString();
        }

        // Convert camelBack / PascalCase name to proper case with spaces (e.g. displayable description from Enum)
        public static string CamelBackToProperCase(this string name) {
            var newName = new StringBuilder();
            newName.Append(name[0]);
            for (var i = 1; i <= name.Length - 2; i++) {
                if (char.IsUpper(name[i]) && char.IsLower(name[i + 1]))
                    newName.Append(' ');
                newName.Append(name[i]);
            }
            newName.Append(name[name.Length - 1]);
            return newName.ToString();
        }

        public static bool IsValidEmail(this string email) {
            try {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch {
                return false;
            }
        }

        public static string NormalizePostcode(this string postcode) {
            postcode = postcode.Trim().ToUpper().Replace(" ", "");
            switch (postcode.Length) {
                case 5: return postcode.Insert(2, " ");
                case 6: return postcode.Insert(3, " ");
                case 7: return postcode.Insert(4, " ");
                default: return postcode;
            }
        }

        #endregion

        #region " DataReader "

        public static bool ContainsColumn(this System.Data.IDataReader reader, string name) {
            for (var i = 0; i < reader.FieldCount; i++) {
                if (reader.GetName(i).Equals(name, StringComparison.CurrentCultureIgnoreCase)) return true;
            }
            return false;
        }

        #endregion

        #region " enum "

        public static string Descripton(this Enum value) {
            var fi = value.GetType().GetField(value.ToString());
            var attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);
            return attributes.Length > 0 ? attributes[0].Description : value.ToString().CamelBackToProperCase();
        }

        public static string Description<TEnum>(this TEnum value) where TEnum : struct, IComparable, IFormattable, IConvertible {
            try {
                var name = value.ToString(CultureInfo.InvariantCulture);
                var attribute = value.GetType().GetField(name).GetCustomAttribute<DescriptionAttribute>(false);
                return attribute != null ? attribute.Description : name;
            }
            catch {
                return "";
            }
        }

        public static Dictionary<TEnum, string> ToDictionary<TEnum>(this Type type) where TEnum : struct, IComparable, IFormattable, IConvertible {
            return Enum.GetValues(type)
                .OfType<TEnum>()
                .ToDictionary(value => value, value => value.Description());
        }

        public static IEnumerable<Model.Lookups.Item> ToLookups<TEnum>(this Type type) where TEnum : struct, IComparable, IFormattable, IConvertible {
            return Enum.GetValues(type)
                .Cast<TEnum>()
                .Select(value => new Model.Lookups.Item {
                    ID = Convert.ToInt32(value),
                    Description = value.Description()
                });
        }

        #endregion
    }
}