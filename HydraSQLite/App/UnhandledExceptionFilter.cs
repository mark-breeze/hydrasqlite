﻿using System;
using System.Diagnostics;
using System.Web.Http.Filters;

namespace HydraSQLite.App {
    public class UnhandledExceptionFilter : ExceptionFilterAttribute {

        private readonly string myAppName;
        private readonly IBreezeConfiguration myConfig;

        public UnhandledExceptionFilter(IBreezeConfiguration config) {
            myConfig = config;
            myAppName = myConfig.BasiliskAppName;
            if (string.IsNullOrWhiteSpace(myAppName)) {
                myAppName = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name;
            }
        }
        
        public override void OnException(HttpActionExecutedContext context) {
            var ex = context.Exception;
            if (Utils.IsDebug()) {
                Debug.WriteLine(ex.ToString());
                return;
            }
            SendReport(ex);
            base.OnException(context);
        }

        public void SendReport(Exception ex) {
            try {
                if (ex is System.Threading.Tasks.TaskCanceledException) return;
                if (ex.InnerException is System.Threading.Tasks.TaskCanceledException) return;

                // if URL not set, just splat it out to debug output
                if (string.IsNullOrEmpty(myConfig.BasiliskUrl)) {
                    Debug.WriteLine("ERROR " + ex);
                    return;
                }

                var errorReport = new Model.ErrorReport() {
                    Application = myAppName,
                    Occurred = DateTime.Now,
                    FullText = ex.ToString(),
                    ExceptionType = ex.GetType().ToString(),
                    Location = FindLocation(ex)
                };

                PostReport(errorReport);
            }
            catch (Exception e) {
                Debug.WriteLine("ERRORRR " + e);
            }
        }

        private void PostReport(Model.ErrorReport report) {
            try {
                var req = System.Net.WebRequest.Create(myConfig.BasiliskUrl);
                req.Method = "POST";
                req.ContentType = "text/json";
                using (var writer = new System.IO.StreamWriter(req.GetRequestStream())) {
                    var json = Newtonsoft.Json.JsonConvert.SerializeObject(report);
                    writer.Write(json);
                    writer.Close();
                }
                // ReSharper disable AssignNullToNotNullAttribute
                req.BeginGetResponse(null, null);
            }
            catch (Exception ex) {
                Debug.WriteLine(ex.ToString());
            }
        }

        private string FindLocation(Exception exception) {
            var location = string.Empty;
            var frame = (new StackTrace(exception, true)).GetFrame(0);
            if (frame != null) {
                var fileName = frame.GetFileName();
                var lineNumber = frame.GetFileLineNumber().ToString();
                if (!string.IsNullOrWhiteSpace(fileName)) {
                    location = string.Concat(fileName, ":", lineNumber);
                }
            }
            return location;
        }
    }
}