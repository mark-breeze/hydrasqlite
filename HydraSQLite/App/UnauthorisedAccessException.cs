﻿using System;

namespace HydraSQLite.App {
    public class UnauthorisedAccessException : UnauthorizedAccessException {
        public UnauthorisedAccessException() { }
        public UnauthorisedAccessException(string message) : base(message) { }
    }
}