﻿using Newtonsoft.Json;

namespace HydraSQLite.Model.Api {

    public class CreateAccountResponse {

        [JsonProperty("status")]
        public UserStatus Status { get; set; } = UserStatus.None;

        [JsonProperty("sentEmail")]
        public bool SentEmail { get; set; }

        [JsonProperty("captchaWrong")]
        public bool CaptchaWrong { get; set; }

        [JsonProperty("captchaExpired")]
        public bool CaptchaExpired { get; set; }
        
        [JsonProperty("isInvalidPassword")]
        public bool IsInvalidPassword { get; set; }

        [JsonProperty("invalidPasswordMessage")]
        public string InvalidPasswordMessage { get; set; }

        [JsonIgnore]
        public string EmailUrlKey { get; set; }
    }
}