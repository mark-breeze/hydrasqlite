﻿using Newtonsoft.Json;

namespace HydraSQLite.Model.Api {

    public class ResetPassword {

        public enum StateType {
            None = 0,
            Ready = 1,
            Expired = 2,
            Success = 3,
            Failed = 4
        }

        [JsonProperty("key")]
        public string Key { get; set; }

        [JsonProperty("state")]
        public StateType State { get; set; }

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("password")]
        public string Password { get; set; }

        [JsonProperty("message")]
        public string Message { get; set; }

        [JsonProperty("validationMessage")]
        public string ValidationMessage { get; set; }

        [JsonIgnore]
        public bool IsSuccess => string.IsNullOrEmpty(ValidationMessage);
    }
}