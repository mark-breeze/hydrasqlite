﻿using Newtonsoft.Json;

namespace HydraSQLite.Model.Api {

    public class CreateAccountRequest {

        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("newPassword")]
        public string Password { get; set; }

        [JsonProperty("forename")]
        public string Forename { get; set; }

        [JsonProperty("surname")]
        public string Surname { get; set; }

        [JsonProperty("captchaID")]
        public string CaptchaID { get; set; }

        [JsonProperty("captchaText")]
        public string CaptchaText { get; set; }

    }
}