﻿using System;
using Newtonsoft.Json;

namespace HydraSQLite.Model {
    public class ErrorReport {
        [JsonProperty("occurred")]
        public DateTime Occurred { get; set; }

        [JsonProperty("exceptionType")]
        public string ExceptionType { get; set; }

        [JsonProperty("application")]
        public string Application { get; set; }

        [JsonProperty("location")]
        public string Location { get; set; }

        [JsonProperty("fullText")]
        public string FullText { get; set; }
    }
}