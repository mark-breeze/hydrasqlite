﻿using System;
using Newtonsoft.Json;
using HydraSQLite.App;

namespace HydraSQLite.Model {
    public class User {

        [JsonProperty("id")]
        public Guid ID { get; set; }

        [JsonIgnore]
        public UserRole Role { get; set; }


        // TODO - can we simplify?
        [JsonProperty("role")]
        public int RoleID {
            get => (int)Role;
            set {
                if (Enum.IsDefined(typeof(UserRole), value))
                    Role = (UserRole)value;
            }
        }

        [JsonProperty("roleDescription")]
        public string RoleDescription => Role.Description();

        [JsonIgnore]
        public UserStatus Status { get; set; }

        [JsonProperty("status")]
        public int StatusID {
            get => (int)Status;
            set {
                if (Enum.IsDefined(typeof(UserStatus), value))
                    Status = (UserStatus)value;
            }
        }
        
        [JsonProperty("email")]
        public string Email { get; set; }

        [JsonProperty("forename")]
        public string Forename { get; set; }

        [JsonProperty("surname")]
        public string Surname { get; set; }

        [JsonProperty("registrationNotes")]
        public string RegistrationNotes { get; set; }
        
        [JsonIgnore]
        public string Password { get; set; }

        [JsonProperty("lastLoggedIn")]
        public DateTime LastLoggedIn { get; set; }

        [JsonProperty("prevLastLoggedIn")]
        public DateTime PrevLastLoggedIn { get; set; }

        [JsonProperty("created")]
        public DateTime Created { get; set; }

        [JsonProperty("createdByUserID")]
        public Guid CreatedByUserID { get; set; }

        [JsonProperty("lastUpdated")]
        public DateTime LastUpdated { get; set; }

        [JsonProperty("lastUpdatedByUserID")]
        public Guid LastUpdatedByUserID { get; set; }

        [JsonProperty("deleted")]
        public DateTime Deleted { get; set; }

        [JsonProperty("deletedByUserID")]
        public Guid DeletedByUserID { get; set; }

        [JsonProperty("newPassword")]
        public string NewPassword { get; set; }
    }
}