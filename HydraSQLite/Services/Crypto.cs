﻿using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace HydraSQLite.Services {

    public class Crypto {

        private static readonly char[] myAvailableTokenCharacters = {
            'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
            'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
            'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
            'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
            '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'
        };

        public static string RandomString(int length) {
            var tokenChars = new char[length];
            var randomData = new byte[length];
            using (var generator = new RNGCryptoServiceProvider()) {
                generator.GetBytes(randomData);
            }
            for (var i = 0; i < tokenChars.Length; i++) {
                var pos = randomData[i] % myAvailableTokenCharacters.Length;
                tokenChars[i] = myAvailableTokenCharacters[pos];
            }
            return new string(tokenChars);
        }

        public static string GetHash(string text) {
            var bytes = Encoding.UTF8.GetBytes(text);
            var hasher = new SHA256Managed();
            var hash = hasher.ComputeHash(bytes);
            return hash.Aggregate(string.Empty, (current, x) => current + x.ToString("x2"));
        }

    }
}