﻿using System;

namespace HydraSQLite.Services.BusinessLogic {

    public class ValidationException : Exception {

        public ValidationException() { }

        public ValidationException(string message) : base(message) { }
    }
}