﻿namespace HydraSQLite.Services.Mockable {

    public interface IFormsAuthentication {

        void SetAuthCookie(string userName, bool createPersistentCookie);
    }
}
