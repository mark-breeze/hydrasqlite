﻿using System;

namespace HydraSQLite.Services.Mockable {

    /// <summary>
    /// System.Runtime.Caching.MemoryCache doesn't implement an interface
    /// so were wrapping here it an providing one so we can inject it and mock it in tests
    /// </summary>
    public class MemoryCacheWrapper : ICache {

        private readonly System.Runtime.Caching.MemoryCache myCache;

        public MemoryCacheWrapper() {
            myCache = System.Runtime.Caching.MemoryCache.Default;
        }
        
        public bool Contains(string key) {
            return myCache.Contains(key);
        }

        public object Get(string key) {
            return myCache.Get(key);
        }

        public void Set(string key, object value, DateTimeOffset absoluteExpiration) {
            myCache.Set(key, value, absoluteExpiration);
        }

        public void Remove(string key) {
            myCache.Remove(key);
        }
    }
}