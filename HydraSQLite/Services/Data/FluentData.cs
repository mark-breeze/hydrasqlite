﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SQLite;
using System.Globalization;
using System.Linq;
using HydraSQLite.App;

// ReSharper disable UnusedAutoPropertyAccessor.Global

namespace HydraSQLite.Services.Data {

    public class FluentData {

        public FluentData(IBreezeConfiguration config) {
            myConnectionString = config.ConnectionString();
        }

        #region internal variables

        private readonly string myConnectionString;
        private SQLiteTransaction myTransaction;
        private string myQueryText = "";
        private CommandType myCommandType = CommandType.Text;
        private int? myTimeout;
        private readonly List<SQLiteParameter> myParameters = new List<SQLiteParameter>();

        #endregion

        #region builder options

        /// <summary>
        /// Short-hand way of setting the stored procedure to execute.
        /// </summary>
        /// <param name="procName">The stored procedure name.</param>
        public FluentData StoredProcedure(string procName) {
            throw new NotSupportedException("SQLite version of this class does not support stored procedures!");
        }

        /// <summary>
        /// Short-hand way of setting the hard-coded SQL query to execute.
        /// </summary>
        /// <param name="text">The SQL query to use.</param>
        public FluentData SqlText(string text) {
            myQueryText = text;
            myCommandType = CommandType.Text;
            return this;
        }

        public FluentData SetTimeout(int timeout) {
            myTimeout = timeout;
            return this;
        }

        public FluentData SetTransaction(SQLiteTransaction trans) {
            myTransaction = trans;
            return this;
        }

        #endregion

        #region parameters

        public FluentData AddParameter(string parameterName, object value) {
            if (string.IsNullOrEmpty(myQueryText)) throw new InvalidOperationException("Please set the query or stored procedure first");
            var parm = new SQLiteParameter(parameterName, GetDbType(value));
            parm.Value = GetDbValue(value, parm.DbType);
            myParameters.Add(parm);
            return this;
        }

        public FluentData AddParameter(string parameterName, DbType dataType, object value) {
            if (string.IsNullOrEmpty(myQueryText)) throw new InvalidOperationException("Please set the query or stored procedure first");
            var parm = new SQLiteParameter(parameterName, dataType) {
                Value = GetDbValue(value, dataType)
            };
            myParameters.Add(parm);
            return this;
        }

        public FluentData AddParameter(string parameterName, SqlDbType dataType, object value) {
            if (string.IsNullOrEmpty(myQueryText)) throw new InvalidOperationException("Please set the query or stored procedure first");
            var parm = new SQLiteParameter(parameterName, dataType) {
                Value = GetDbValue(value, dataType)
            };
            myParameters.Add(parm);
            return this;
        }

        public FluentData AddParameter(string parameterName, DbType dataType, ParameterDirection direction) {
            if (string.IsNullOrEmpty(myQueryText)) throw new InvalidOperationException("Please set the query or stored procedure first");
            var parm = new SQLiteParameter(parameterName, dataType) {
                Direction = direction
            };
            myParameters.Add(parm);
            return this;
        }

        public FluentData AddParameters(IEnumerable<DbParameter> parms) {
            if (string.IsNullOrEmpty(myQueryText)) throw new InvalidOperationException("Please set the query or stored procedure first");
            foreach (var parm in parms) {
                var newParm = new SQLiteParameter(parm.ParameterName, parm.DbType) {
                    Value = GetDbValue(parm.Value, parm.DbType)
                };
                myParameters.Add(newParm);
            }
            return this;
        }

        public T GetParameterValue<T>(string parameterName) {
            var parm = myParameters.FirstOrDefault(p => string.Equals(p.ParameterName, parameterName, StringComparison.OrdinalIgnoreCase));
            return NonNullValue<T>(parm?.Value);
        }

        // modified for SQLite
        private DbType GetDbType(object value) {
            if (value is byte) return DbType.Int16;
            if (value is short) return DbType.Int16;
            if (value is int) return DbType.Int32;
            if (value is long) return DbType.Int64;
            if (value is DateTime) return DbType.String;
            if (value is string) return DbType.String;
            if (value is char) return DbType.String;
            if (value is bool) return DbType.Int16;
            if (value is Guid) return DbType.String;
            if (value is byte[]) return DbType.String;
            if (value is double || value is float) return DbType.Double;
            if (value is decimal) return DbType.Double;
            return DbType.String;
        }

        private object GetDbValue(object value, DbType dbType) {
            if (value == DBNull.Value) return value;
            if (value == null) return DBNull.Value;
            return dbType == DbType.DateTime || dbType == DbType.DateTime2 ? SqlDateForDatabase((DateTime)value) : value;
        }

        private object GetDbValue(object value, SqlDbType dbType) {
            if (value == DBNull.Value) return value;
            if (value == null) return DBNull.Value;
            return dbType == SqlDbType.DateTime || dbType == SqlDbType.DateTime2 ? SqlDateForDatabase((DateTime)value) : value;
        }

        private object SqlDateForDatabase(DateTime value) {
            if (value < System.Data.SqlTypes.SqlDateTime.MinValue.Value) {
                return DBNull.Value;
            }
            // if already set to local, assume is correct value
            if (value.Kind == DateTimeKind.Local) {
                return value;
            }
            // if not local, add correct offset to convert UTC to local time
            //   - e.g., will be 0 for GMT or +1h for BST
            var offset = TimeZoneInfo.Local.GetUtcOffset(value);
            return DateTime.SpecifyKind(value, DateTimeKind.Local) + offset;
        }

        #endregion

        #region execution

        public FluentData ExecuteNonQuery() {
            using (var cmd = GetCommand()) {
                try {
                    if (cmd.Connection.State != ConnectionState.Open) cmd.Connection.Open();
                    cmd.ExecuteNonQuery();
                }
                finally {
                    if (cmd.Connection.State == ConnectionState.Open && myTransaction == null) cmd.Connection.Close();
                }
                return this;
            }
        }

        /// <summary>
        /// Return the value of a single column and row. Most likely primitive value
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <returns></returns>
        public T ExecuteScalar<T>() {
            using (var cmd = GetCommand()) {
                try {
                    if (cmd.Connection.State != ConnectionState.Open) cmd.Connection.Open();
                    return (T)Convert.ChangeType(cmd.ExecuteScalar(), typeof(T));
                }
                finally {
                    if (cmd.Connection.State == ConnectionState.Open && myTransaction == null) cmd.Connection.Close();
                }
            }
        }

        /// <summary>
        /// Return a collection of results
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="unwrapperFunc">Mapping function that turns reader into object instance (e.g. Repository.Load(IDataReader reader)</param>
        /// <returns>IEnumerable of model objects</returns>
        public List<T> ExecuteReader<T>(Func<IDataReader, T> unwrapperFunc) {
            using (var cmd = GetCommand()) {
                try {
                    if (cmd.Connection.State != ConnectionState.Open) cmd.Connection.Open();
                    var retVal = new List<T>();
                    using (var reader = cmd.ExecuteReader())
                        while (reader.Read()) {
                            retVal.Add((T) Convert.ChangeType(unwrapperFunc.Invoke(reader), typeof(T)));
                        }

                    return retVal;
                }
                catch (Exception ex) {
                    System.Diagnostics.Debug.WriteLine(ex.ToString());
                    throw;
                }
                finally {
                    if (cmd.Connection.State == ConnectionState.Open && myTransaction == null) cmd.Connection.Close();
                }
            }
        }

        /// <summary>
        /// Return a collection of results
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="unwrapperFunc">Mapping function that turns reader into object instance (e.g. Repository.Load(IDataReader reader)</param>
        /// <param name="totalCountColumnName">Name of column in result set that contains the TOTAL count (assuming we're just getting a page of results.)</param>
        /// <returns>IEnumerable of model objects</returns>
        public (int count, List<T> list) ExecuteReader<T>(Func<IDataReader, T> unwrapperFunc, string totalCountColumnName) {
            using (var cmd = GetCommand()) {
                try {
                    if (cmd.Connection.State != ConnectionState.Open) cmd.Connection.Open();
                    var list = new List<T>();
                    var count = 0;
                    var isFirst = true;
                    using (var reader = cmd.ExecuteReader()) {
                        if (!reader.ContainsColumn(totalCountColumnName)) {
                            throw new ArgumentException("Column not found:" + totalCountColumnName);
                        }
                        while (reader.Read()) {
                            if (isFirst) {
                                count = NonNullValue<int>(reader[totalCountColumnName]);
                                isFirst = false;
                            }
                            list.Add((T)Convert.ChangeType(unwrapperFunc.Invoke(reader), typeof(T)));
                        }
                    }
                    return (count, list);
                }
                finally {
                    if (cmd.Connection.State == ConnectionState.Open && myTransaction == null) cmd.Connection.Close();
                }
            }
        }

        /// <summary>
        /// Return a collection of results. *** Warning *** You MUST close the reader manually.
        /// </summary>
        /// <returns>IDataReader - result set from query</returns>
        public IDataReader ExecuteReader() {
            using (var cmd = GetCommand()) {
                if (cmd.Connection.State != ConnectionState.Open) cmd.Connection.Open();
                return cmd.ExecuteReader();
            }
        }

        public class AnonymousReaderOptions {
            public bool ParseColumnPrefixes { get; set; }

            public bool FirstCharacterToLowerCase { get; set; }
        }

        /// <summary>
        /// Return a collection of anonymous objects, built up by the SQL queries SELECT columns.
        /// </summary>
        /// <returns>List of anonymous objects</returns>
        public List<object> ExecuteAnonymousReader() {
            return ExecuteAnonymousReader(new AnonymousReaderOptions());
        }

        /// <summary>
        /// Return a collection of anonymous objects, built up by the SQL queries SELECT columns. Custom options for developer to choose.
        /// </summary>
        /// <returns>List of anonymous objects</returns>
        public List<object> ExecuteAnonymousReader(AnonymousReaderOptions readOptions) {
            using (var cmd = GetCommand()) {
                var retVal = new List<object>();
                try {
                    if (cmd.Connection.State != ConnectionState.Open) cmd.Connection.Open();
                    using (var reader = cmd.ExecuteReader())
                        while (reader.Read()) {
                            IDictionary<string, object> newItm = new Dictionary<string, object>();
                            for (var i = 0; i < reader.FieldCount; i++) {
                                var columnName = readOptions.ParseColumnPrefixes
                                    ? reader.GetName(i).Substring(3, reader.GetName(i).Length - 3)
                                    : reader.GetName(i);
                                if (readOptions.FirstCharacterToLowerCase)
                                    columnName = char.ToLowerInvariant(columnName[0]) + columnName.Substring(1);

                                newItm.Add(columnName, NonNullValue(reader[i]));
                            }
                            //var eo = new ExpandoObject();
                            //var eoColl = (ICollection<KeyValuePair<string, object>>) eo;
                            //foreach (var kvp in newItm)
                            //    eoColl.Add(kvp);

                            //dynamic eoDynamic = eo;
                            //retVal.Add(eoDynamic);
                            retVal.Add(newItm);
                        }
                }
                finally {
                    if (cmd.Connection.State == ConnectionState.Open && myTransaction == null) cmd.Connection.Close();
                }
                return retVal;
            }
        }

        private object NonNullValue(object value) {
            if (value is string) return NonNullValue<string>(value);
            if (value is int) return NonNullValue<int>(value);
            if (value is DateTime) return NonNullValue<DateTime>(value);
            if (value is bool) return NonNullValue<bool>(value);
            if (value is decimal) return NonNullValue<decimal>(value);
            if (value is float) return NonNullValue<float>(value);
            if (value is double) return NonNullValue<double>(value);
            if (value is Guid) return NonNullValue<Guid>(value);
            return NonNullValue<string>(value);
        }

        public static T NonNullValue<T>(object value) {
            if (value == null || value == DBNull.Value) {
                if (typeof(T) == typeof(string)) {
                    //special case for string - we don't want null, we want empty string
                    return (T)Convert.ChangeType(string.Empty, typeof(T), CultureInfo.InvariantCulture);
                }
                if (typeof(T) == typeof(DateTime)) {
                    // special case for date - we want zero date BUT for it to be local, not unspecified!
                    return (T)Convert.ChangeType(new DateTime(0, DateTimeKind.Local), typeof(T), CultureInfo.InvariantCulture);
                }
                return default(T);
            }
            try {
                if (typeof(T) == typeof(DateTime) && value is string dateText) {
                    // for sqlite dates
                    var isOk = DateTime.TryParse(dateText, out var parsedValue);
                    value = DateTime.SpecifyKind( isOk ? parsedValue : DateTime.MinValue, DateTimeKind.Local);
                }
                else if (typeof(T) == typeof(DateTime)) {
                    value = DateTime.SpecifyKind((DateTime)value, DateTimeKind.Local);
                }
                else if (typeof(T) == typeof(Guid) && value is string guidText) {
                    // for sqlite guid IDs
                    var isOk = Guid.TryParse(guidText, out var guid);
                    value = isOk ? guid : Guid.Empty;
                }
                return (T)Convert.ChangeType(value, typeof(T), CultureInfo.InvariantCulture);
            }
            catch { return default(T); }
        }

        /// <summary>
        /// Return an individual result.
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="unwrapperFunc"></param>
        /// <returns>Single instance of T or NULL</returns>
        public T ExecuteResult<T>(Func<IDataReader, T> unwrapperFunc) {
            using (var cmd = GetCommand()) {
                try {
                    if (cmd.Connection.State != ConnectionState.Open) cmd.Connection.Open();
                    using (var reader = cmd.ExecuteReader())
                        if (reader.Read()) {
                            return (T)Convert.ChangeType(unwrapperFunc.Invoke(reader), typeof(T));
                        }
                }
                finally {
                    if (cmd.Connection.State == ConnectionState.Open && myTransaction == null) cmd.Connection.Close();
                }

                return default(T);
            }
        }

        /// <summary>
        /// Create and return a transaction for processing a number of queries in batch.
        /// </summary>
        /// <param name="action">The transaction logic to invoke.</param>
        public void ExecuteTransaction(Action<SQLiteTransaction> action) {
            SQLiteConnection conn = null;
            SQLiteTransaction trans = null;

            try {
                conn = GetConnection();
                if (conn.State != ConnectionState.Open) conn.Open();
                trans = conn.BeginTransaction();

                action.Invoke(trans);
                trans.Commit();
            }
            catch {
                trans?.Rollback();
                throw; //rethrow
            }
            finally {
                if (conn != null && conn.State == ConnectionState.Open) conn.Close();
            }
        }

        /// <summary>
        /// Create and return a transaction for processing a number of queries in batch. Return a result.
        /// </summary>
        /// <typeparam name="T">The datatype to return</typeparam>
        /// <param name="transactionFunc">The transaction logic to invoke. Must return an instance of T</param>
        /// <returns></returns>
        public T ExecuteTransaction<T>(Func<SQLiteTransaction, T> transactionFunc) {
            SQLiteConnection conn = null;
            SQLiteTransaction trans = null;
            T result;
            try {
                conn = GetConnection();
                if (conn.State != ConnectionState.Open) conn.Open();
                trans = conn.BeginTransaction();

                result = (T)Convert.ChangeType(transactionFunc.Invoke(trans), typeof(T));
                trans.Commit();
            }
            catch {
                trans?.Rollback();
                throw; //rethrow
            }
            finally {
                if (conn != null && conn.State == ConnectionState.Open) conn.Close();
            }
            return result;
        }

        private SQLiteCommand GetCommand() {
            var cmd = myTransaction == null
                ? GetConnection().CreateCommand()
                : myTransaction.Connection.CreateCommand();

            cmd.CommandText = myQueryText;
            cmd.CommandType = myCommandType;

            if (myTransaction != null) cmd.Transaction = myTransaction;
            if (myTimeout.HasValue) cmd.CommandTimeout = myTimeout.Value;

            foreach (var parm in myParameters) {
                cmd.Parameters.Add(parm);
            }

            return cmd;
        }

        private SQLiteConnection GetConnection() {
            if (myTransaction != null) return myTransaction.Connection;
            return new SQLiteConnection(myConnectionString);
        }

        #endregion

    }

}