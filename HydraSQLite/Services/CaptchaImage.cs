﻿using System;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Drawing.Imaging;
using System.Linq;

// ReSharper disable UnusedAutoPropertyAccessor.Global
// ReSharper disable MemberCanBePrivate.Global

namespace HydraSQLite.Services {
    /// <summary>
    /// CAPTCHA image generation class
    /// </summary>
    /// <remarks>
    /// Adapted from 
    /// http://www.codeproject.com/aspnet/CaptchaImage.asp
    /// Jeff Atwood
    /// http://www.codinghorror.com/
    /// </remarks>
    public class CaptchaImage {

        private readonly Color myColour = Color.FromArgb(102, 102, 102);

        private readonly Color myBgColour = Color.FromArgb(245, 245, 245); 
        private int myHeight;
        private int myWidth;
        private readonly Random myRand;
        private string myRandomText;
        private int myRandomTextLength;
        private string myRandomTextChars;
        private string myFontFamilyName;

        private string myFontWhitelist;
        #region "  Public Enums"

        /// <summary>
        /// Amount of random font warping to apply to rendered text
        /// </summary>
        public enum FontWarpFactor {
            None,
            Low,
            Medium,
            High,
            Extreme
        }

        /// <summary>
        /// Amount of background noise to add to rendered image
        /// </summary>
        public enum BackgroundNoiseLevel {
            None,
            Low,
            Medium,
            High,
            Extreme
        }

        /// <summary>
        /// Amount of curved line noise to add to rendered image
        /// </summary>
        public enum LineNoiseLevel {
            None,
            Low,
            Medium,
            High,
            Extreme
        }

        #endregion

        #region "  Public Properties"

        /// <summary>
        /// Returns an ID that uniquely identifies this Captcha
        /// </summary>
        public string UniqueId { get; }

        /// <summary>
        /// Returns the date and time this image was last rendered
        /// </summary>
        public DateTime RenderedAt { get; }

        /// <summary>
        /// Font family to use when drawing the Captcha text. If no font is provided, a random font will be chosen from the font whitelist for each character.
        /// </summary>
        public string Font {
            get { return myFontFamilyName; }
            set {
                try {
                    var font1 = new Font(value, 12f);
                    myFontFamilyName = value;
                    font1.Dispose();
                }
                catch (Exception) {
                    myFontFamilyName = FontFamily.GenericSerif.Name;
                }
            }
        }

        /// <summary>
        /// Amount of random warping to apply to the Captcha text.
        /// </summary>
        public FontWarpFactor FontWarp { get; set; }

        /// <summary>
        /// Amount of background noise to apply to the Captcha image.
        /// </summary>
        public BackgroundNoiseLevel BackgroundNoise { get; set; }

        public LineNoiseLevel LineNoise { get; set; }

        /// <summary>
        /// A string of valid characters to use in the Captcha text. 
        /// A random character will be selected from this string for each character.
        /// </summary>
        public string TextChars {
            get { return myRandomTextChars; }
            set {
                myRandomTextChars = value;
                myRandomText = GenerateRandomText();
            }
        }

        /// <summary>
        /// Number of characters to use in the Captcha text. 
        /// </summary>
        public int TextLength {
            get { return myRandomTextLength; }
            set {
                myRandomTextLength = value;
                myRandomText = GenerateRandomText();
            }
        }

        /// <summary>
        /// Returns the randomly generated Captcha text.
        /// </summary>
        public string Text => myRandomText;

        /// <summary>
        /// Width of Captcha image to generate, in pixels 
        /// </summary>
        public int Width {
            get { return myWidth; }
            set {
                if ((value <= 60)) {
                    throw new ArgumentOutOfRangeException(nameof(value), value, "width must be greater than 60.");
                }
                myWidth = value;
            }
        }

        /// <summary>
        /// Height of Captcha image to generate, in pixels 
        /// </summary>
        public int Height {
            get { return myHeight; }
            set {
                if (value <= 30) {
                    throw new ArgumentOutOfRangeException(nameof(value), value, "height must be greater than 30.");
                }
                myHeight = value;
            }
        }

        /// <summary>
        /// A semicolon-delimited list of valid fonts to use when no font is provided.
        /// </summary>
        public string FontWhitelist {
            get { return myFontWhitelist; }
            set { myFontWhitelist = value; }
        }

        #endregion

        public CaptchaImage() {
            myRand = new Random();
            FontWarp = FontWarpFactor.Medium;
            BackgroundNoise = BackgroundNoiseLevel.Low;
            LineNoise = LineNoiseLevel.Low;
            myWidth = 180;
            myHeight = 50;
            myRandomTextLength = 5;
            myRandomTextChars = "ACDEFGHJKLNPQRTVXYZ2346789";
            myFontFamilyName = "";
            // -- a list of known good fonts in on both Windows XP and Windows Server 2003
            myFontWhitelist = "arial;arial black;comic sans ms;courier new;estrangelo edessa;franklin gothic medium;" + "georgia;lucida console;lucida sans unicode;mangal;microsoft sans serif;palatino linotype;" + "sylfaen;tahoma;times new roman;trebuchet ms;verdana";
            myRandomText = GenerateRandomText();
            RenderedAt = DateTime.Now;
            UniqueId = GenerateID(32);
        }

        /// <summary>
        /// Forces a new Captcha image to be generated using current property value settings.
        /// </summary>
        public Bitmap RenderImage() {
            return GenerateImagePrivate();
        }

        /// <summary>
        /// Returns a random font family from the font whitelist
        /// </summary>
        string[] myStaticRandomFontFamilyFf;
        private string RandomFontFamily() {
            //-- small optimization so we don't have to split for each char
            if (myStaticRandomFontFamilyFf == null) {
                myStaticRandomFontFamilyFf = myFontWhitelist.Split(';');
            }
            return myStaticRandomFontFamilyFf[myRand.Next(0, myStaticRandomFontFamilyFf.Length)];
        }

        /// <summary>
        /// generate random text for the CAPTCHA
        /// </summary>
        private string GenerateRandomText() {
            var sb = new System.Text.StringBuilder(myRandomTextLength);
            var maxLength = myRandomTextChars.Length;
            for (var n = 0; n <= myRandomTextLength - 1; n++) {
                sb.Append(myRandomTextChars.Substring(myRand.Next(maxLength), 1));
            }
            return sb.ToString();
        }

        /// <summary>
        /// Returns a random point within the specified x and y ranges
        /// </summary>
        private PointF RandomPoint(int xmin, int xmax, int ymin, int ymax) {
            return new PointF(myRand.Next(xmin, xmax), myRand.Next(ymin, ymax));
        }

        /// <summary>
        /// Returns a random point within the specified rectangle
        /// </summary>
        private PointF RandomPoint(Rectangle rect) {
            return RandomPoint(rect.Left, rect.Width, rect.Top, rect.Bottom);
        }

        /// <summary>
        /// Returns a GraphicsPath containing the specified string and font
        /// </summary>
        private GraphicsPath TextPath(string s, Font f, Rectangle r) {
            var sf = new StringFormat();
            sf.Alignment = StringAlignment.Near;
            sf.LineAlignment = StringAlignment.Near;
            var gp = new GraphicsPath();
            gp.AddString(s, f.FontFamily, Convert.ToInt32(f.Style), f.Size, r, sf);
            return gp;
        }

        /// <summary>
        /// Returns the CAPTCHA font in an appropriate size 
        /// </summary>
        private Font GetFont() {
            float fsize = 0;
            var fname = myFontFamilyName;
            if (string.IsNullOrEmpty(fname)) {
                fname = RandomFontFamily();
            }
            switch (FontWarp) {
                case FontWarpFactor.None:
                    fsize = Convert.ToInt32(myHeight * 0.7);
                    break;
                case FontWarpFactor.Low:
                    fsize = Convert.ToInt32(myHeight * 0.8);
                    break;
                case FontWarpFactor.Medium:
                    fsize = Convert.ToInt32(myHeight * 0.8);
                    break;
                case FontWarpFactor.High:
                    fsize = Convert.ToInt32(myHeight * 0.8);
                    break;
                case FontWarpFactor.Extreme:
                    fsize = Convert.ToInt32(myHeight * 0.8);
                    break;
            }
            return new Font(fname, fsize, FontStyle.Bold);
        }

        /// <summary>
        /// Renders the CAPTCHA image
        /// </summary>
        private Bitmap GenerateImagePrivate() {
            Font fnt = null;
            var bmp = new Bitmap(myWidth, myHeight, PixelFormat.Format32bppArgb);
            var gr = Graphics.FromImage(bmp);
            gr.SmoothingMode = SmoothingMode.AntiAlias;

            //-- fill an empty white rectangle
            var rect = new Rectangle(0, 0, myWidth, myHeight);
            var br = new SolidBrush(myBgColour);
            gr.FillRectangle(br, rect);

            var charOffset = 0;
            // ReSharper disable once PossibleLossOfFraction
            double charWidth = myWidth / myRandomTextLength;

            foreach (var c in myRandomText) {
                //-- establish font and draw area
                fnt = GetFont();
                var rectChar = new Rectangle(Convert.ToInt32(charOffset * charWidth), 0, Convert.ToInt32(charWidth), myHeight);

                //-- warp the character
                var gp = TextPath(c.ToString(), fnt, rectChar);
                WarpText(gp, rectChar);

                //-- draw the character
                br = new SolidBrush(myColour);
                gr.FillPath(br, gp);

                charOffset += 1;
            }

            AddNoise(gr, rect);
            AddLine(gr, rect);

            //-- clean up unmanaged resources
            fnt?.Dispose();
            br.Dispose();
            gr.Dispose();

            return bmp;
        }

        /// <summary>
        /// Warp the provided text GraphicsPath by a variable amount
        /// </summary>
        private void WarpText(GraphicsPath textPath, Rectangle rect) {
            float warpDivisor = 0;
            float rangeModifier = 0;

            switch (FontWarp) {
                case FontWarpFactor.None:
                    return;
                case FontWarpFactor.Low:
                    warpDivisor = 6;
                    rangeModifier = 1;
                    break;
                case FontWarpFactor.Medium:
                    warpDivisor = 5;
                    rangeModifier = 1.3f;
                    break;
                case FontWarpFactor.High:
                    warpDivisor = 4.5f;
                    rangeModifier = 1.4f;
                    break;
                case FontWarpFactor.Extreme:
                    warpDivisor = 4;
                    rangeModifier = 1.5f;
                    break;
            }

            var rectF = new RectangleF(Convert.ToSingle(rect.Left), 0, Convert.ToSingle(rect.Width), rect.Height);

            var hrange = Convert.ToInt32(rect.Height / warpDivisor);
            var wrange = Convert.ToInt32(rect.Width / warpDivisor);
            var left = rect.Left - Convert.ToInt32(wrange * rangeModifier);
            var top = rect.Top - Convert.ToInt32(hrange * rangeModifier);
            var width = rect.Left + rect.Width + Convert.ToInt32(wrange * rangeModifier);
            var height = rect.Top + rect.Height + Convert.ToInt32(hrange * rangeModifier);

            if (left < 0)
                left = 0;
            if (top < 0)
                top = 0;
            if (width > Width)
                width = Width;
            if (height > Height)
                height = Height;

            var leftTop = RandomPoint(left, left + wrange, top, top + hrange);
            var rightTop = RandomPoint(width - wrange, width, top, top + hrange);
            var leftBottom = RandomPoint(left, left + wrange, height - hrange, height);
            var rightBottom = RandomPoint(width - wrange, width, height - hrange, height);

            var points = new[] {
                leftTop,
                rightTop,
                leftBottom,
                rightBottom
            };
            var m = new Matrix();
            m.Translate(0, 0);
            textPath.Warp(points, rectF, m, WarpMode.Perspective, 0);
        }


        /// <summary>
        /// Add a variable level of graphic noise to the image
        /// </summary>
        private void AddNoise(Graphics graphics1, Rectangle rect) {
            var density = 0;
            var size = 0;

            switch (BackgroundNoise) {
                case BackgroundNoiseLevel.None:
                    return;
                case BackgroundNoiseLevel.Low:
                    density = 30;
                    size = 40;
                    break;
                case BackgroundNoiseLevel.Medium:
                    density = 18;
                    size = 40;
                    break;
                case BackgroundNoiseLevel.High:
                    density = 16;
                    size = 39;
                    break;
                case BackgroundNoiseLevel.Extreme:
                    density = 12;
                    size = 38;
                    break;
            }

            var br = new SolidBrush(myColour);
            var max = Convert.ToInt32(Math.Max(rect.Width, rect.Height) / size);

            for (var i = 0; i <= Convert.ToInt32((rect.Width * rect.Height) / density); i++) {
                graphics1.FillEllipse(br, myRand.Next(rect.Width), myRand.Next(rect.Height), myRand.Next(max), myRand.Next(max));
            }
            br.Dispose();
        }

        /// <summary>
        /// Add variable level of curved lines to the image
        /// </summary>

        private void AddLine(Graphics graphics1, Rectangle rect) {
            var length = 0;
            float width = 0;
            var linecount = 0;

            switch (LineNoise) {
                case LineNoiseLevel.None:
                    return;
                case LineNoiseLevel.Low:
                    length = 4;
                    width = Convert.ToSingle(myHeight / 31.25);
                    // 1.6
                    linecount = 1;
                    break;
                case LineNoiseLevel.Medium:
                    length = 5;
                    width = Convert.ToSingle(myHeight / 27.7777);
                    // 1.8
                    linecount = 1;
                    break;
                case LineNoiseLevel.High:
                    length = 3;
                    width = Convert.ToSingle(myHeight / 25);
                    // 2.0
                    linecount = 2;
                    break;
                case LineNoiseLevel.Extreme:
                    length = 3;
                    width = Convert.ToSingle(myHeight / 22.7272);
                    // 2.2
                    linecount = 3;
                    break;
            }

            var pf = new PointF[length + 1];
            var p = new Pen(myColour, width);

            for (var l = 1; l <= linecount; l++) {
                for (var i = 0; i <= length; i++) {
                    pf[i] = RandomPoint(rect);
                }
                graphics1.DrawCurve(p, pf, 1.75f);
            }

            p.Dispose();
        }

        private static readonly Random myRandom = new Random();

        private string GenerateID(int length) {
            const string chars = "ZbBe4QslzE9R2y1NP0u8Yk36tdGjSrvmCFKHWiJnAUhIaqcXOpogLwD5fx7MTV";
            return new string(Enumerable.Repeat(chars, length)
              .Select(s => s[myRandom.Next(s.Length)]).ToArray());
        }


    }

}