# Breeze Hydra (SQLite)

This is Breeze's 'seed' application for new web applications.  This version has been adapted to run on
SQLite rather than the usual SQL Server database.  Basic functionality includes:

  * Registering, authenticating and authorising users.
  * Editing users (CRUD)
  * Editing lookups which may be used for configurable drop-down options and such.
  * Editing a list of companies

All these features would tend to be used in a new application except for the companies edit feature 
which is just there so that the sample app actually does something.

## General Architecture

There are three parts to the project that could in theory be three (or maybe two) separate projects - 
or be written using different technologies.  They are however all in one project because it's easier 
to manage. The parts are:

  * HTML pages and CSS using ASP.Net MVC, SASS and Bootstrap
  * Client side script client using Knockout.JS and Typescript
  * Database and API using ASP.Net Web API and SQL Server (or SQLite in this case)

For HTML, etc, the application uses ASP.Net MVC - but only for generating fairly static pages which are used as
templates for the client side code to work upon.  The M part of MVC (on the server) isn't really used apart
from some minimal code for passing variables such as a mapped root URL to the client scripts, etc.  All
the other 'dynamic' parts of the app are implemented via Knockout JS, viewmodels and calls to a JSON API.

The client side code does not operate as a 'single page application' as such but you can consider the 
individual pages as 'islands of SPA' as they operate in a SPAish manner but without all the routing
etc.  Basically what happens is:

  * A page is requested and HTML, CSS and Javascript is loaded in the usual way.
  * A view-model is instantiated which then loads data from the API into a set of Knockout observables (using instances of model classes where necessary) via the Knockout.Mapping library.
  * The view-model is data-bound to the DOM (or possibly just appropriate parts of it)
  * Via user interactions (e.g. data-bind="click:..."), the view-model makes API calls and updates itself and the data on the server
  * On page navigation the view-model is destroyed and the process starts again...

The link between the database and the API is via our own system of model classes and a repository 
pattern.  We have a code generator for writing a lot of the boiler-plate code that this involves.  
We do not use Entity Framework partly because we have traditionally had good SQL skills and we want 
to make the best of those.

## Tools Required

To run the project, you will need:

  * A Windows machine (since we're using .NET Framework, not Core)
  * Git (https://gitforwindows.org/)
  * Visual Studio 2017 (Community edition will do - https://visualstudio.microsoft.com/downloads/)
  * .NET Framework 4.7.2 Developer Pack (https://dotnet.microsoft.com/download/visual-studio-sdks)

You may also want:

  * smtp4dev (a Windows version here https://github.com/rnwood/smtp4dev/releases/tag/v2.0.10)
  * Web Compiler - if you want to edit SCSS files and have them automatically to CSS  (https://marketplace.visualstudio.com/items?itemName=MadsKristensen.WebCompiler)
  * npm - if you want to update any client-side packages - although all the ones you need should be in the project.
  * Gulp - if you want updated npm packages to be pulled into the project

...but these are entirely optional and you can manage without them.

## Getting Started

I've tried these steps out on a fresh Windows 10 machine.

1) Download and install:
    * Git for Windows 
    * Visual Studio 2107 Community Edition - with web development (ASP.Net) option - if need be.
    * .Net 4.7.2 from https://dotnet.microsoft.com/download/thank-you/net472-developer-pack

2) Open a command line prompt in a folder that you keep development projects.

3) Enter command `git clone https://bitbucket.org/mark-breeze/HydraSQLite.git` to download the project.

4) Open the solution in Visual Studio.  Assuming that the project opens, you may need to change the startup
project from **HydraSQLite.Tests** to **HydraSQLite**.

5) Run HydraSQLite (debug).

You may see this error:

```
Could not find a part of the path 'C:\Users\xxx\Documents\HydraSQLite\HydraSQLite\bin\roslyn\csc.exe
```

...in which case, run this in the package manager console `Update-Package Microsoft.CodeDom.Providers.DotNetCompilerPlatform -r`.  
The Package Manager Console can be accessed if it is not showing via: Menu > View > Other Windows.

At this point, you should see the project running in a browser.  You can login via the 
backdoor shortcuts or you can sign in with mark.hodgson@breeze-it.com and breeze**.








