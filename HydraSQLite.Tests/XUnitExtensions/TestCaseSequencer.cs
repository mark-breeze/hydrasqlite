﻿using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using Xunit.Abstractions;
using Xunit.Sdk;

namespace Tests.XUnitExtensions {

    // from http://www.tomdupont.net/2016/04/how-to-order-xunit-tests-and-collections.html
    public class TestCaseSequencer : ITestCaseOrderer {

        public static readonly ConcurrentDictionary<string, ConcurrentQueue<string>> QueuedTests = new ConcurrentDictionary<string, ConcurrentQueue<string>>();

        public IEnumerable<TTestCase> OrderTestCases<TTestCase>(IEnumerable<TTestCase> testCases) where TTestCase : ITestCase {
            return testCases.OrderBy(GetOrder);
        }

        private static int GetOrder<TTestCase>(TTestCase testCase) where TTestCase : ITestCase {
            // Enqueue the test name.
            QueuedTests
                .GetOrAdd(testCase.TestMethod.TestClass.Class.Name, key => new ConcurrentQueue<string>())
                .Enqueue(testCase.TestMethod.Method.Name);

            // Order the test based on the attribute.
            var attr = testCase.TestMethod.Method
                .ToRuntimeMethod()
                .GetCustomAttribute<SequenceAttribute>();
            return attr?.Sequence ?? 0;
        }
    }
}
