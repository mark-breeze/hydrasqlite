﻿using System;
using HydraSQLite.Services;
using Moq;
using Tests.Mocks;

namespace Tests {

    // https://xunit.github.io/docs/shared-context.html#class-fixture
    public class SharedServicesFixture : IDisposable {

        public SharedServicesFixture() {
            Config = new Config();
            Cache = new Cache();
            CurrentUser = new Mocks.CurrentUser();
            EmailUrlKey = new EmailUrlKey(Config);
            UserRepository = new UserRepository {Config = Config};
            
            // set up a basic stub emailer
            var emailerMock = new Mock<IEmailer>();
            emailerMock.Setup(e => e.SendEmail(It.IsAny<string>(), It.IsAny<string>(), It.IsAny<string>())).ReturnsAsync(true);
            Emailer = emailerMock.Object;

            Login = new Login(Config, Emailer, Cache, EmailUrlKey, UserRepository, CurrentUser);
        }

        public Config Config { get; }
        public IEmailer Emailer { get; }
        public Cache Cache { get; }
        public Mocks.CurrentUser CurrentUser { get; }
        public IEmailUrlKey EmailUrlKey { get; }
        public IUserRepository UserRepository { get; }
        public ILogin Login { get; }

        public void Dispose() {
            System.Diagnostics.Debug.WriteLine("@@@ SharedServicesFixture Dispose");
            Cache.ResetAll();
        }
    }
}
